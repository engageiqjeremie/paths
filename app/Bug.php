<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Bug extends Model
{
    protected $fillable = [
        'name',
        'description',
        //'assignto',
        'created_by',
        'updated_by',
        'status'
    ];
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
