<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailRecipient extends Model
{

    protected $fillable = [
        'email',
        'full_name'
    ];
}
