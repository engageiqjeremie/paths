<?php

namespace App\Http\Controllers;

use Auth;
use JavaScript;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'clearance']);

    }
    /**
     * Dashboard area
     */
    public function index()
    {

        // $user = Auth::user();
        // $signedIn = Auth::check();
        //
        // $array = [
        //     'csrfToken' => csrf_token(),
        //     'apiToken' => $user->api_token,
        //     'signedIn' => $signedIn,
        //     'role' => $user->getRoleNames()
        // ];
        //
        // // dd($array);
        //
        // JavaScript::put($array);

		return view('dashboard');
    }
}
