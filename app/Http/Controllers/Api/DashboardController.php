<?php

namespace App\Http\Controllers\Api;

use Auth;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'clearance']);
    }

    /**
     * show specific path
     */
    public function show()
    {

    }
}
