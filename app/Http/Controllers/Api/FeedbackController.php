<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Session\Store;

use App\Http\Controllers\Controller;

class FeedbackController extends Controller
{
    /**
     * Instantiate and determine if path is active.
     *
     */
    public function __construct ()
    {

    }

    /**
     * [emailRecipients description]
     * @param  Request           $request        [description]
     * @param  AppEmailRecipient $emailRecipient [description]
     * @return [type]                            [description]
     */
    public function emailRecipients(
        Request $request,
        \App\EmailRecipient $emailRecipient
    ) {
        $rows = $emailRecipient->all();

        $emails = [];
        foreach($rows as $index => $email) {
            $emails[$index]['label'] = $email->full_name;
            $emails[$index]['value'] = $email->email;
            $emails[$index]['text'] = $email->email;
        }

        return $emails;
    }

    /**
     * [addEmailRecipients description]
     * @param Request           $request        [description]
     * @param AppEmailRecipient $emailRecipient [description]
     */
    public function addEmailRecipients(
        Request $request,
        \App\EmailRecipient $emailRecipient
    ) {
        try {
            $emailRecipient->full_name = $request->get('full_name');
            $emailRecipient->email = $request->get('email');
            $emailRecipient->save();

            return ['error' => false, 'msg' => ''];
        } catch (\Exception $error) {
        return ['error' => true, 'msg' => $error->getMessage()];
        }
    }
}
