<?php

namespace App\Http\Controllers\Api;

use Cache;

use Illuminate\Http\Request;

use App\Events\PathsCreated;
use App\Events\PathsUpdated;
use App\Http\Requests\PathRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\PathResource;

class PathController extends Controller
{
    protected $path;

    public function __construct(
        \App\Http\Services\Repo\Path $path
    ) {
        // $this->middleware(['auth', 'clearance']);

        $this->path = $path;
    }

    /**
    * Get all paths
    *
    * @return App\Http\Resources\PathResource
    */
    public function lists()
    {
        // return Cache::remember('paths', 60, function () {
        // 	return  PathResource::collection($this->path->all());
        // });

        return  PathResource::collection($this->path->all());
    }

    /**
    * Create new path
    *
    * @param  PathRequest                          				$request
    * @param  App\Http\Services\Repo\Options       				$options
    * @param  App\Http\Services\Paths\Shift\PathController     $pathController
    * @param  App\Http\Services\Paths\Shift\PathBlade          $pathBlade
    * @param  App\Http\Services\Paths\Shift\PathRoute          $pathRoute
    * @param  App\Http\Services\Paths\Shift\PathStyle          $pathStyle
    * @return void
    */
    public function create (
        PathRequest $request,
        \App\Http\Services\Repo\Option $option,
        \App\Http\Services\Paths\Shift\PathController $pathController,
        \App\Http\Services\Paths\Shift\PathBlade $pathBlade,
        \App\Http\Services\Paths\Shift\PathRoute $pathRoute,
        \App\Http\Services\Paths\Shift\PathStyle $pathStyle
    ) {
        // Add to database
        $path = $this->path->create($request);

        $slug = strtolower(str_replace(' ', '-', $request->input('name')));
        $controllerName = $this->controllerName($request->input('name'));

        // Create route
        $pathRoute->create($slug, $controllerName);
        // Create contoller
        $pathController->create($slug, $controllerName);
        // Create blade
        $pathBlade->create($slug);
        // Create stylesheet
        $pathStyle->create($slug);

        Cache::forget('paths');
        Cache::remember('paths', 60, function () {
            return  $this->path->all();
        });

        $option->store($this->path->pathID(), [
                'section' =>'themes',
                'template' => 'default'
            ]
        );

        event(new PathsCreated($path));

        // Add ability to use production file in local environment
        if(getEnvironmentFolderName() == 'devpaths') {
                $option->store($this->path->pathID(), [
                    'section' =>'general',
                    'use_production_file' => 'false'
                ]
            );
            return new PathResource($path);
        }

        // Execute version control
        // $cmd = 'chmod +x ' . base_path() . '/sh/add.path.sh';
        $cmd = 'sh ' . base_path() . '/sh/add.path.sh';
        $cmd .= " '" . $slug ."'";
        $cmd .= " '" . base_path() . "'";
        $cmd .= " 'Paths'";
        $cmd .= " '" . $controllerName . "'";
        $cmd .= " '" . resource_path() . "'";



        $cmd .= " 'paths'";
        $cmd .= " 'Adding'";
        $cmd .= " '" . ucwords($request->input('name')) . "'";
        $cmd .= " '" . config('leadreactor.git_username') . "'";

        exec($cmd);

        return new PathResource($path);
    }

    /**
    * Delete path
    *
    * @param  App\Http\Services\Paths\Shift\PathController     $pathController
    * @param  App\Http\Services\Paths\Shift\PathBlade          $pathBlade
    * @param  App\Http\Services\Paths\Shift\PathRoute          $pathRoute
    * @param  App\Http\Services\Paths\Shift\$pathStorage       $pathStorage
    * @param  App\Http\Services\Paths\Shift\PathStyle          $pathStyle
    * @param  Path                         						$path
    * @param  integer                     						$pathID
    * @return array
    */
    public function delete(
        \App\Http\Services\Paths\Shift\PathController $pathController,
        \App\Http\Services\Paths\Shift\PathBlade $pathBlade,
        \App\Http\Services\Paths\Shift\PathRoute $pathRoute,
        \App\Http\Services\Paths\Shift\PathStorage $pathStorage,
        \App\Http\Services\Paths\Shift\PathStyle $pathStyle,
        $pathID
    ) {
        // Check path exists in dtabase by id
        if($this->path->has($pathID)) {
            // Delete the path
            $this->path->delete();

            $controllerName = $this->controllerName($this->path->name());
            $slug = $this->path->slug();

            // Remove route
            $pathRoute->remove($slug);
            // Remove controller
            $pathController->remove($controllerName);
            // Remove blade
            $pathBlade->remove($slug);
            // Remove stylesheet
            $pathStyle->remove($slug);
            // Remove Storage
            $pathStorage->remove($slug);

            Cache::forget('paths');
            Cache::remember('paths', 60, function () {
                return  $this->path->all();
            });

            // Execute version control
            if(getEnvironmentFolderName() != 'devpaths') {
                // Execute version control
                $cmd = 'sh ' . base_path() . '/sh/add.path.sh';
                $cmd .= " '" . $slug ."'";
                $cmd .= " '" . base_path() . "'";
                $cmd .= " 'Paths'";
                $cmd .= " '" . $controllerName . "'";
                $cmd .= " '" . resource_path() . "'";
                $cmd .= " 'paths'";
                $cmd .= " 'Removing'";
                $cmd .= " '" .  ucwords($this->path->name()) . "'";
                $cmd .= " '" . config('leadreactor.git_username') . "'";

                exec($cmd);
            }

            return [
                'error' => false
            ];
        }

        return [
            'error' => true,
            'msg' => 'Unable to remove the path co\'z it\'s unavailable.'
        ];
    }

    /**
    * Get path details
    *
    * @param  integer  $pathID
    * @return Illuminate\Http\JsonResponse
    */
    public function get ($pathID)
    {
        return response()->json(Cache::remember('path-details-' . $pathID, 60, function () {
                return  new PathResource($this->path->getDetails($pathID));
            })
        );
    }
        
    /**
    * Update pth getDetails
    *
    * @param  Request $request
    * @param  Path    $path
    * @param  integer  $pathID
    * @return array
    */
    public function update(
        Request $request,
        $pathID
    ) {
        // Check path exists in dtabase by id
        if($this->path->has($pathID)) {
            // Update path
            if($path = $this->path->update($request->all())) {

                Cache::forget('paths');
                Cache::forget('path-details-' . $pathID);

                Cache::remember('paths', 60, function () {
                    return  $this->path->all();
                });
                Cache::remember('path-details-' . $pathID, 60, function () {
                    return $this->path->getDetails($pathID);
                });

                event(new PathsUpdated($path));

                return ['error' => false];
            }
        }

        return [
            'error' => true,
            'msg' => 'Can\'t update for the path is unavailable.'
        ];

    }

    /**
    * Update path status
    *
    * @param  Request $request
    * @param  integer  $id
    * @return Illuminate\Http\Response
    */
    public function updateStatus(
        Request $request,
        $pathID
    ) {
        // Update path status
        $this->path->update([
            'status' => ($request->input('status')  == 1) ? 0: 1
        ], $pathID);

        Cache::forget('paths');
        Cache::remember('paths', 60, function () {
            return  $this->path->all();
        });

        return response($this->path->details());
    }

    /**
    * Set controller name
    *
    * @param  string $name
    * @return string
    */
    protected function controllerName ($name)
    {
        return str_replace(' ', '', ucwords($name)) . 'Controller';
    }
}
