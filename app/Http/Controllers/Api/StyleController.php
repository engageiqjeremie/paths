<?php

namespace App\Http\Controllers\Api;

use App\Option;

use Illuminate\Http\Request;
use Illuminate\Filesystem\Filesystem;

use App\Http\Services\Paths\Preview;

use App\Http\Controllers\Controller;

class StyleController extends Controller
{
    protected $style;

    public function __construct(
        Filesystem $file,
        \App\Http\Services\Style $style
        )
    {
        $this->file = $file;
        $this->style = $style;
    }


    public function get(
        Request $request,
        $slug
        )
    {

        $editor = resource_path() . '/assets/sass/paths/' . $slug . '/_editor.scss';
        $backup = resource_path() . '/assets/sass/paths/' . $slug . '/_backup.scss';
        $temp = resource_path() . '/assets/sass/paths/' . $slug . '/_temp.scss';

        return [
                'editor' => $this->file->get($editor),
                'editor_size' => $this->file->size($editor),
                'backup' => $this->file->get($backup),
                'backup_size' => $this->file->size($backup),
                'temp_size' => $this->file->size($temp)
            ];
    }

    public function update(
        Request $request,
        \App\Http\Services\Repo\Option $option,
        $pathID
        )
    {
        // echo '<pre>';
        // print_r($request->all());
        // echo '</pre>';
        // return;
        // Update editor stylesheet
        $this->file->put($this->style->getResourcePath($request->get('slug'). '/_editor.scss') , $request->get('content'));

        // Update stylesheet for compliling
        $this->style->updateStylesheet(
            $request->get('slug'),
            ($color = $request->get('color')) ? $color: '#c2cfd6',
            $request->get('template'),
            ($request->get('progress_bar_type') == 'custom') ? '@import \'../../progressbar/' . $request->get('custom_file_name') . '\';' : ''
        );

        // compile to public path
        $response = $this->style->compile($this->style->getResourcePath($request->get('slug'). '/'), $this->style->getPublicPath());

        // Update vesion to render new stylesheet, not from cache.
        if(!$response['error']) {
            $option->store(
                $pathID, ['section' => 'style', 'version' => 'v' . time()]
            );
        }

        return $response;
    }

    public function backupStylesheet(
        Request $request,
        $slug
        )
    {
        return $this->style->backup($slug);
    }

    public function revertStylesheetBackup(
        Request $request,
        $slug
        )
    {
        return $this->style->revert($slug);
    }

    public function applyReversionStylesheetBackup(
        Request $request,
        Option $option,
        $pathID
        )
    {
        $insert = [
            'path_id' => $pathID,
            'key' => 'version',
            'value' => 'v' . time() ,
            'section' => 'style'
        ];
        $option->updateOrCreate(['key' => 'version', 'path_id' => $pathID], $insert);

        $slug = $request->get('slug');

        return $this->style->applyReversion($slug);
    }

    public function cancelReversionStylesheetBackup(
        Request $request,
        $slug
        )
    {
        return $this->style->cancelReversion($slug);
    }
}
