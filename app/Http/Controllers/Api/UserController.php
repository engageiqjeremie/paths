<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests\UserRequest;
use App\Http\Requests\UpdateRoleRequest;

use Illuminate\Support\Facades\Mail;
use App\Mail\PleaseConfirmYourEmail;

use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     *  guest users can't see this route
     */
    public function __construct(
        \App\Http\Services\Repo\User $user
        )
    {
        $this->middleware(['auth', 'isSystemAdministrator'])->except(['index']);

        $this->user = $user;
    }
    /**
     * show users
     * @var resource
     */
    public function index()
    {
        $users = $this->user->all();

        return response()->json([
                'pagination' => [
                    'total' => $users->total(),
                    'per_page' => $users->perPage(),
                    'current_page' => $users->currentPage(),
                    'last_page' => $users->lastPage(),
                    'from' => $users->firstItem(),
                    'to' => $users->lastItem()
                ],
                'data' => $users
            ]
        );
    }

    /**
     * show users
     * @var resource
     */
    public function getUser($userID)
    {
        return $this->user->get($userID);
    }

    /**
     * create new user
     */
    public function createUser(UserRequest $request)
    {
        $password = str_random(strlen($request->input('password')));

        $user = $this->user->create($request, $password);

        Mail::to($user)->send(new PleaseConfirmYourEmail($user, $password));

        return response()->json($user);
    }

    /**
     *  edit user
     * @param $request Request
     * @param $id int
     */
    public function edit(UpdateRoleRequest $request, $userID)
    {
        $this->user->updateRole($request, $userID);
    }

    /**
     * deactivate a User
     * Once deactivated, User no longer access in any guarded routes
     * @var $request Request
     */
    public function deactivateUser(Request $request, $userID)
    {
        return $this->user->updateStatus($request->input('status'), $userID);
    }
}
