<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Bug;

use App\Http\Controllers\Controller;

class UsersTaskController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * return all users tasks
     */
    public function index()
    {
        return Bug::with('user')->paginate(5);
    }
}
