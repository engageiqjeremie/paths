<?php

namespace App\Http\Controllers;

use JavaScript;

use Illuminate\Http\Request;
use Illuminate\Session\Store;

use App\Http\Controllers\Controller;

class FeedbackController extends Controller
{
    /**
     * Traits
     */
    use \App\Http\Traits\Store;

    /**
     *  Required fields
     *
     * @var string
     */
    protected $params = [
        'first_name' => '',
        'last_name' => '',
        'email' => '',
        'address' => '',
        'city' => '',
        'state' => '',
        'zip' => '',
        'category_type' => '',
        'feedback' => '',
        'error' => '',
        'zip_status' => ''
    ];

    /**
     * Instantiate and determine if path is active.
     *
     * @param Illuminate\Session\Store      $store
     */
    public function __construct (
        Store $store
        )
    {
        // $this->middleware('isPathActive');

        // Store to property
        // Use by Traits\Store
        $this->store = $store;
    }

    /**
     * Feedback page
     *
     * @param  Illuminate\Http\Request      $request
     * @param  string                       $slug
	 * @return Illuminate\View\View
     */
    public function feedback (
        Request $request,
        $slug
        )
    {
        // Redirect to landing page when the path slug in url was change ..
        // if($this->fetchStore('session_slug') != $this->fetchStore('path.slug')) {
        if($request->segment(2) != $this->fetchStore('path.slug')) {
            // forget filter question in order for landing page to call an api fo it again.
            $this->store->forget('filter_questions');

            return  redirect()->route($slug .'-landing', $request->all());
        };

        $params = array_merge($this->params, $request->all());
        if($this->storeHas('user')) $params = array_merge($params, $this->fetchStore('user'));

        if($request->has('mail_error')) $params['error'] = $request->get('mail_error') . '.  Please Try again later.';

        if($request->has('comments')) $params['category_type'] = $request->get('comments');
        if($request->has('feedback')) $params['feedback'] = $request->get('feedback');

        return view('feedback')->with($params);
    }

    /**
     * [feedbackVue description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function feedbackVue (
        Request $request
        )
    {
        $params = array_merge($this->params, $request->all());
        $campaigns = ($this->storeHas('campaigns')) ? $this->fetchStore('campaigns'): [];
        $currentPage = ($request->has('current_page'))? $this->fetchStore('nlr.path_url') . '/' . $request->get('current_page') : '';
        $emailRecipients = 'jeremie@engageiq.com';
        if($this->Storehas('path.options.feedback.email_recipients')) {
            $emailRecipients = $this->fetchStore('path.options.feedback.email_recipients');
        }

        if($this->storeHas('user')) $params = array_merge($params, $this->fetchStore('user'));


        $params = $this->setParams($params, $campaigns, $currentPage,  $request->ip(), $emailRecipients, false);

        JavaScript::put(['user' => $params]);

        return view('vue')->with($params);
    }

    /**
     * [testVue description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function processFeedbackVue(Request $request)
    {
        $params = $request->all();
        $emailRecipients = $params['email_recipients'];

        // Do the Email
        try {
            \Mail::send(
                'emails.feedback',
                $params,
                function ($mail) use($emailRecipients) {
                    $mail->from('noreply@engageiq.com')
                         ->to($emailRecipients)
                         ->subject('New feedback for a survey page on NLR');
                }
            );
        }
        catch (\Exception $exception) {
            return [
                'error' => true,
                'msg' => $exception->getMessage()
            ];
        }

        return [
            'error' => false
        ];
    }

    /**
     * Process user's feedback
     *
     * @param  Illuminate\Http\Request      $request
     * @param  string                       $slug
	 * @return Illuminate\View\View
     */
    public function processFeedback (
        Request $request,
        $slug
        )
    {
        if($request->get('zip_status') == 'invalid') return $this->feedback($request, $slug);

        $campaigns = ($this->storeHas('campaigns')) ? $this->fetchStore('campaigns'): [];
        $currentPage = ($request->has('current_page'))? $this->fetchStore('nlr.path_url') . '/' . $request->get('current_page') : '';
        $emailRecipients = 'jeremie@engageiq.com';
        if($this->Storehas('path.options.feedback.email_recipients')) {
            $emailRecipients = $this->fetchStore('path.options.feedback.email_recipients');
        }

        $params = $this->setParams ($request->all(), $campaigns, $currentPage,  $request->ip(), $emailRecipients);

        // Do the Email
        try {
            \Mail::send(
                'emails.feedback',
                $params,
                function ($mail) use($emailRecipients) {
                    $mail->from('noreply@engageiq.com')
                         ->to($emailRecipients)
                         ->subject('New feedback for a survey page on NLR');
                }
            );
        }
        catch (\Exception $exception) {
            $request->merge(['mail_error' => $exception->getMessage()]);
        }

        return $this->feedback($request, $slug);
    }

    /**
     * [setParams description]
     * @param array $request   [description]
     * @param array $campaigns [description]
     */
    protected function setParams (
        $request = [],
        $campaigns = [],
        $currentPage = '',
        $userIP = '',
        $emailRecipients = 'jeremie@engageiq.com',
        $sessionJson = 1
        )
    {
        $params = array_merge($this->params, $request);

        if($this->storeHas('user')) $params = array_merge($params, $this->fetchStore('user'));

        $params['last_visited'] = $this->fetchStore('_previous.url');
        if($currentPage) $params['last_visited'] = $currentPage;

        $params['browser_info'] = 'Using ' . $this->fetchStore('nlr.browser.browser') . " " . $this->fetchStore('nlr.browser.browser_version') .' on ' . $this->fetchStore('nlr.browser.user_agent');
        $params['php_session'] = $this->storeID();
        if($sessionJson) $params['session_json'] = json_encode($this->stored());
        $params['category_type'] = (array_key_exists('comments', $request)) ? $request['comments'] : '';
        $params['feedback'] = (array_key_exists('feedback', $request)) ? $request['feedback'] : '';
        $params['addcode'] = 'CD' . (($this->fetchStore('revenue_tracker_id')) ? $this->fetchStore('revenue_tracker_id') : 1);
        $params['ip'] = $userIP;

        $pNum = 0;
        $pagePath = '';
        if(count($campaigns)) {
          foreach($campaigns as $content) {
            $pagePath .= 'Page ' . ++ $pNum . ': ' . implode(', ', $content) . '<br>';
          }
        }
        $params['page_path'] = $pagePath;
        $params['email_recipients'] = $emailRecipients;

        return $params;
    }
}
