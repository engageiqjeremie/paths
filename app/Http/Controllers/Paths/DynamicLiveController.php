<?php

namespace App\Http\Controllers\Paths;

use Illuminate\Http\Request;
use App\Http\Controllers\Paths\DefaultPathController;

class DynamicLiveController extends DefaultPathController
{

    const PATH_SLUG = 'dynamic-live';

    /**
     * [path description]
     * @param  [type] $page   [description]
     * @param  [type] $params [description]
     * @return [type]         [description]
     */
    protected function path($page, $params)
    {
        return view('paths.dynamic-live.'.$page)->with($params);
    }

    /**
     * [redirect2Campaign description]
     * @return [type] [description]
     */
    protected function redirect2Campaign()
    {
        return redirect()->route('dynamic-live-campaign', [1]);
    }
}
