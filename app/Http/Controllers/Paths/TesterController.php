<?php

namespace App\Http\Controllers\Paths;

use Illuminate\Http\Request;
use App\Http\Controllers\Paths\DefaultPathController;

class TesterController extends DefaultPathController
{

    const PATH_SLUG = 'tester';

    /**
     * Determine the blade to use.
     *
     * @param  string $page
     * @param  array $params
     * @return Illuminate\View\View
     */
    protected function path($page, $params)
    {
        return view('paths.tester.'.$page)->with($params);
    }

    /**
     * Redirect to correct route/
     *
     * @return Illuminate\Http\RedirectResponse
     */
    protected function redirect2Campaign()
    {
        return redirect()->route('tester-campaign', [1]);
    }
}
