<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Filesystem\Filesystem;

use App\Http\Services\Paths\Preview;

class TemplateController extends Controller
{
    /**
     * [__construct description]
     */
    public function __construct ()
    {
    }

    /**
     * [landing description]
     * @param  Request        $request
     * @param  NLRToken       $tokenHandler
     * @param  DefaultLanding $default
     * @return [type]
     */
    public function preview(
        Request $request,
        Preview $preview,
        $template
        )
    {
        $preview->setParams([
            'template' => $template
        ]);

		return view('templates.preview')->with($preview->params());
    }

	/**
	* Preview image in storage
	*
	* @param  Request                $request
	* @param  AppHttpServicesStorage $storage
	* @param  string                 $slug
	* @param  string                 $image
	* @return void
	*/
	public function previewImage(
		Request $request,
		\App\Http\Services\Storage $storage,
		$slug,
		$image
	) {
		if(!$storage->exists($slug . '/' . $image)) return abort('404');

		header("Content-type: image/png");
		echo $storage->getImage($slug . '/' . $image);
	}
}
