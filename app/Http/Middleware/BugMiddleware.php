<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class BugMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //users with this permission only
        if (Auth::user()->hasPermissionTo('Administer Bugs')) {
            //when a user is creating bug
            if ($request->is('api/bugs')) {
                if (!Auth::user()->hasPermissionTo('Create Bug')) {
                    return response('UnAuthorized', 401);
                } else {
                    return $next($request);
                }
            }
            //when a user editing a bugs
            if ($request->isMethod('put') || $request->isMethod('patch')) {
                if (!Auth::user()->hasPermissionTo('Edit Bug')) {
                    return response('UnAuthorized', 401);
                } else {
                    return $next($request);
                }
            }
            //if user is deleting a bug
            if ($request->isMethod('delete')) {
                if (!Auth::user()->hasPermissionTo('Delete Bug')) {
                    return response('UnAuthorized', 401);
                } else {
                    return $next($request);
                }
            }
        }
        return $next($request);
    }
}
