<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class NlrTokenMiddleware
{
    protected $tokenHandler;

    public function __construct(
        \App\Http\Services\NLRToken $tokenHandler
        )
    {
        $this->tokenHandler = $tokenHandler;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!config('leadreactor.token_applied')) return $next($request);

        if(!$request->session()->has('nlr.leadreactor_token')) $this->getToken($request);

        return $next($request);
    }

    protected function getToken ($request)
    {
        // This settings was copied from old paidforresearch.com
        $this->tokenHandler->setEncryptionApllied(config('leadreactor.encryption_applied'))
                           ->setEncryptionKey(config('leadreactor.encryption_key'))
                           ->setMacKey(config('leadreactor.mac_key'))
                           ->setTokenEmail(config('leadreactor.token_email'))
                           ->setTokenPassword(config('leadreactor.token_password'))
                           ->setTokenUrl(config('leadreactor.lead_reactor_url'));

        $request->session()->put('nlr.leadreactor_token', $this->tokenHandler->getToken());

        // If curl throws exceptions
        if($this->tokenHandler->hasError()) $request->merge(['token_error' => $this->tokenHandler->error()]);
    }
}
