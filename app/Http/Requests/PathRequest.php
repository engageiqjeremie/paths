<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Support\Facades\Validator;

class PathRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        Validator::extend('not_default', function($attribute, $value, $parameters)
        {
            // Banned words
            if (strtolower($value) == 'default') return false;
            return true;
        });

        return [
            'name' => 'required|not_default|unique:paths|max:255',
            'description' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.not_default' => 'Try another name.'
        ];
    }
}
