<?php

/**
 * Simple Class Method Illustration
 *
 *
 * @category  PHP
 * @author    Jeremie S. Yunsay <jeremie@engageiq.com>
 * @copyright 2018 Jeremie S. Yunsay
 */

namespace App\Http\Services\Helpers;

final class Browser extends \hisorange\BrowserDetect\Parser
{
    protected $types = [
        'isDesktop'  => [
            'type' => 'Desktop',
            'view' => 1
        ],
        'isMobile'  => [
            'type' => 'Mobile',
            'view' => 2
        ],
        'isTablet'  => [
            'type' => 'Tablet',
            'view' => 3
        ]
    ];

    public function type($key)
    {
        return $this->types[$key]['type'];
    }

    public function view($key)
    {
        return $this->types[$key]['view'];
    }

    public function device ()
    {
        return [
            'isMobile'  => $this->isMobile(),
            'isTablet'  => $this->isTablet(),
            'isDesktop' => (!$this->isMobile() && !$this->isTablet()) ? true : false
        ];
    }

    public function browser ()
    {
        return [
            'os' => $this->platformName(),
            'os_version'=> $this->platformVersion(),
            'browser' => $this->browserName(),
            'browser_version' => $this->browserVersion(),
            'user_agent' => $this->userAgent()
        ];
    }
}
