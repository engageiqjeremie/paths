<?php

/**
* Get folder name
*
* @return string
*/
function getEnvironmentFolderName ()
{
    if(app('env') == 'production') return 'paths';
    return 'devpaths';
}
