<?php
namespace  App\Http\Services\Paths;

class DefaultRegistration extends DefaultLanding
{
    protected $mobileDetect = [];

    public function __construct (
        Options $options
        )
    {
        $this->options = $options;
    }

    public function setParams ($inputs)
    {
        $this->updateDefaultParams($inputs);
    }
}
