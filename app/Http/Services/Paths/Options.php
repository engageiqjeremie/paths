<?php

namespace  App\Http\Services\Paths;

use App\Option;

class Options
{
    /**
     * Traits
     */
    use \App\Http\Traits\PathFile;

    /**
     * Container for path options
     *
     * @var array
     */
    protected $options = [];

    /**
     * Id of path
     *
     * @var integer
     */
    protected $pathID = 1;

    /**
     * Default options
     *
     * @var array
     */
    protected $defaultOptions = [];

    /**
    * Instantiate
    *
    * @param App\Option $option
    */
    public function __construct(
        \Illuminate\Filesystem\Filesystem $file
        )
    {
        $this->file = $file;
    }

    /**
     * [get description]
     * @return [type] [description]
     */
    public function get()
    {
        return $this->options;
    }

    /**
     * Setting of options as arguments then replace to default options
     *
     */
    public function set($options = [])
    {
        $template = 'default';

        if(!is_array($options)) $options = [];

        if(array_key_exists('themes', $options)) {
            if(array_key_exists('template', $options['themes'])) $template = $options['themes']['template'];
        }

        $path = $this->getPath('view', 'templates/' . $template . '/options.ini');

        if($this->file->exists($path)) {
                $this->defaultOptions = $this->parseIniFile($path, true);
        }

        $this->options = array_replace_recursive($this->defaultOptions, $options);

        $this->unserialized();
    }

    /**
     * Values to array
     *
     */
    protected function unserialized () {
        if(!array_key_exists('trackers', $this->options)) return;

        if($this->options['trackers']['files']) $this->options['trackers']['files'] = explode(',', $this->options['trackers']['files']);
    }

    /**
     * [parseIniFile description]
     * @param  [type]  $file            [description]
     * @param  integer $processSections [description]
     * @param  [type]  $scannerMode     [description]
     * @return [type]                   [description]
     */
    protected function parseIniFile($file, $processSections = 0, $scannerMode = INI_SCANNER_NORMAL) {
        $explodeStr = '.';
        $escapeChar = "'";
        // load ini file the normal way
        $data = parse_ini_file($file, $processSections, $scannerMode);

        foreach ($data as $sectionKey => $section) {
            // // // loop inside the section
            foreach ($section as $key => $value) {
                if (strpos($key, $explodeStr)) {
                    if (substr($key, 0, 1) !== $escapeChar) {
                        // key has a dot. Explode on it, then parse each subkeys
                        // and set value at the right place thanks to references
                        $subKeys = explode($explodeStr, $key);
                        if(end($subKeys) == 'name') {
                            $indx = $value;
                            $data[$sectionKey][$value] = '';
                        }
                        if(end($subKeys) == 'default') $data[$sectionKey][$indx] = $value;

                        // unset the dotted key, we don't need it anymore
                        unset($data[$sectionKey][$key]);
                    }
                }
            }
        }
        return $data;
    }
}
