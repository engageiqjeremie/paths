<?php

namespace  App\Http\Services\Paths\Shift;

use Illuminate\Filesystem\Filesystem;

class PathBlade
{
    /**
     * Traits
     */
    use \App\Http\Traits\PathFile;

    /**
     * Lists of blades
     *
     * @var array
     */
    protected $blades = [
        'campaign',
        'landing',
        'question',
        'registration'
    ];

    public function __construct (Filesystem $file)
    {
        $this->file = $file;
    }

    /**
     * [create description]
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function create ($slug)
    {
        $path = $this->getPath('view', strtolower($this->getEnvironmentFolderName()) . '/' . $slug);

        $this->createFolderIfNotExists($path);

        foreach ($this->blades as $blade) {
            $stub = $this->file->get($this->getStub('view', $this->getPath('view')));

            $stub = str_replace('dummyBladeName', $blade, $stub);
            $stub = str_replace('dummySlug', $slug, $stub);

            $this->file->put($path . '/' . $blade . '.blade.php', $stub);
        }
    }

    /**
     * [remove description]
     * @param  [type] $slug       [description]
     * @return [type]             [description]
     */
    public function remove($slug)
    {
        $this->file->deleteDirectory($this->getPath('view', strtolower($this->getEnvironmentFolderName()) . '/' . $slug));
    }
}
