<?php

namespace  App\Http\Services\Paths\Shift;

use Illuminate\Support\Str;

use Illuminate\Filesystem\Filesystem;

class PathController
{
    /**
     * Traits
     */
    use \App\Http\Traits\PathFile;

    /**
     * [__construct description]
     * @param Filesystem $file [description]
     */
    public function __construct (Filesystem $file)
    {
        $this->file = $file;
    }

    /**
     * [create description]
     * @param  [type] $slug           [description]
     * @param  [type] $controllerName [description]
     * @return [type]                 [description]
     */
    public function create ($slug, $controllerName)
    {
        $path = $this->getPath('controller', $controllerName . '.php');

        $this->createFolderIfNotExists($this->getPath('controller'));

        $stub = $this->file->get($this->getStub('controller'));

        // // Parse input name.
        $name = str_replace('Controller', '', $controllerName);

        // Create replacements
        $viewPathName = strtolower($this->getEnvironmentFolderName()) . '.' . strtolower(preg_replace('/([a-zA-Z])(?=[A-Z])/', '$1-', str_replace('\\', '.', $name)));

        // Apply replacements
        $stub = str_replace('DummyFolderName', ucwords($this->getEnvironmentFolderName()), $stub);
        $stub = str_replace('DummyClass', $controllerName, $stub);
        $stub = str_replace('DummyViewPath', $viewPathName, $stub);
        $stub = str_replace('DummySlug', $slug, $stub);

        $this->file->put($path, $stub);
    }

    /**
     * [remove description]
     * @param  [type] $slug       [description]
     * @return [type]             [description]
     */
    public function remove($controllerName)
    {
        $this->file->delete($this->getPath('controller', $controllerName . '.php'));
    }
}
