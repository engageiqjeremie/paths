<?php

namespace  App\Http\Services\Paths\Shift;

use Illuminate\Filesystem\Filesystem;

class PathRoute
{
    /**
     * Traits
     */
    use \App\Http\Traits\PathFile;

    public function __construct (Filesystem $file)
    {
        $this->file = $file;
    }

    /**
     * [create description]
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function create ($slug, $controllerName)
    {
        $path = $this->getPath('route', $slug . '.php');

        $this->createFolderIfNotExists($this->getPath('route'));

        $stub = $this->file->get($this->getStub('route', $this->routeStubPath()));

        $stub = str_replace('dummyFolderName', ucwords($this->getEnvironmentFolderName()), $stub);
        $stub = str_replace('dummySlug', $slug, $stub);
        $stub = str_replace('dummyControllerName', $controllerName, $stub);

        $this->file->put($path, $stub);
    }

    /**
     * [remove description]
     * @param  [type] $slug       [description]
     * @param  [type] $folderName [description]
     * @return [type]             [description]
     */
    public function remove($slug)
    {
        $this->file->delete($this->getPath('route', $slug . '.php'));
    }
}
