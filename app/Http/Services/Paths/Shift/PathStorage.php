<?php

namespace App\Http\Services\Paths\Shift;

use Illuminate\Filesystem\Filesystem;

class PathStorage
{
    /**
     * Traits
     */
    use \App\Http\Traits\PathFile;

    public function __construct (Filesystem $file)
    {
        $this->file = $file;
    }


    /**
     * [create description]
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function create ($slug)
    {
        $path = $this->getPath('storage', $slug);

        $this->createFolderIfNotExists($path);
    }

    /**
     * [remove description]
     * @param  [type] $slug       [description]
     * @param  [type] $folderName [description]
     * @return [type]             [description]
     */
    public function remove($slug)
    {
        $this->file->deleteDirectory($this->getPath('storage', $slug));
    }
}
