<?php

namespace  App\Http\Services\Paths\Shift;

use scssc;

use Illuminate\Filesystem\Filesystem;

class PathStyle extends \App\Http\Services\Style
{
    /**
     * Traits
     */
    use \App\Http\Traits\PathFile;

    /**
     * [__construct description]
     * @param Filesystem $file [description]
     */
    public function __construct (Filesystem $file)
    {
        $this->file = $file;
    }

    /**
     * [create description]
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function create ($slug)
    {
        $resourcePath = $this->getResourcePath($slug);
        $publicPath = $this->getPublicPath();

        $this->createFolderIfNotExists($resourcePath);
        $this->createFolderIfNotExists($publicPath);

        $stub = $this->getStubAndReplace('#3097D1', 'default');

        $this->file->put($resourcePath . '/' . $slug . '.min.scss', $stub);
        $this->file->put($resourcePath . '/_editor.scss', '');
        $this->file->put($resourcePath . '/_backup.scss', '');
        $this->file->put($resourcePath . '/_temp.scss', '');

        $this->compile($resourcePath . '/', $publicPath);
    }

    /**
     * [remove description]
     * @param  [type] $slug       [description]
     * @param  [type] $folderName [description]
     * @return [type]             [description]
     */
    public function remove($slug)
    {
        $this->file->deleteDirectory($this->getResourcePath($slug));
        // $this->file->deleteDirectory($this->getPublicPath($slug));
        $this->file->delete($this->getPublicPath($slug . '.min.css'));
    }

    /**
     * [getStubAndReplace description]
     * @param  [type] $color       [description]
     * @param  [type] $template    [description]
     * @param  string $progressbar [description]
     * @return [type]              [description]
     */
    protected function getStubAndReplace ($color, $template, $progressbar = '')
    {
        $stub = $this->file->get($this->getStub('style'));
        $stub = str_replace('dummyColor',$color, $stub);
        $stub = str_replace('dummyTemplate', $template, $stub);
        $stub = str_replace('dummyProgressBar', $progressbar, $stub);

        return $stub;
    }
}
