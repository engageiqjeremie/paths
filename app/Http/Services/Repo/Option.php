<?php

namespace App\Http\Services\Repo;

use App\Option as Model;

use Illuminate\Http\Request;

class Option
{
    /**
     * Model
     *
     * @var App\Option
     */
    protected $model;

    /**
     * Container for path options
     *
     * @var array
     */
    protected $options = [];

    /**
    * Instantiate
    *
    * @param App\Option $option
    */
    public function __construct(
        Model $model
        )
    {
        $this->model = $model;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function find($pathID)
    {
        return  $this->model->find($pathID);
    }

    /**
     * Getch options
     *
     * @return array
     */
    public function options()
    {
        return $this->options;
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get($pathID = '', $key = '')
    {
        if(count($this->options)) return $this->options;

        if($pathID) {
            $this->find($pathID);
            $this->get();
        }

        if($pathID && $key) {
            $this->model->where('key', $key)->find($pathID);
            $this->get();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function remove($pathID, $key)
    {
        return $this->model->where('path_id', $pathID)->where('key', $key)->delete();
    }


    /**
     * [saveInputs description]
     * @param  [type] $pathID   [description]
     * @param  [type] $options  [description]
     * @param  array  $included [description]
     * @return [type]           [description]
     */
    public function store ($pathID, $options, $included = [])
    {
        foreach ($options as  $key => $value) {
            if($this->dontStoreThis($pathID, $key, $value, $included)) continue;

            if(is_array($value)) $value = serialize($value);

            $this->model->updateOrCreate(
                [
                    'key' => $key,
                    'path_id' => $pathID
                ],
                [
                    'path_id' => $pathID,
                    'key' => $key,
                    'value' => $value,
                    'section' => $options['section']
                ]
            );
        }
    }

    /**
     * [dontStoreThis description]
     * @param  [type]  $pathID   [description]
     * @param  [type]  $key      [description]
     * @param  [type]  $value    [description]
     * @param  array   $included [description]
     * @return boolean           [description]
     */
    protected function dontStoreThis($pathID, $key, $value, $included = [])
    {
        if($key == 'section' || $key == 'slug') return true;

        if(count($included)) if(!in_array($key, $included)) return true;

        if($value == null  || $value == '' || $value == 'undefined') {
            $this->model->where('key', $key)->where('path_id', $pathID)->delete();
            return true;
        }

        return false;
    }
}
