<?php

namespace  App\Http\Services\Repo;

use App\Path as Model;

use Illuminate\Http\Request;

class Path
{
    /**
     * [protected description]
     * @var [type]
     */
    protected $model;

    /**
     * [protected description]
     * @var [type]
     */
    protected $path;

    /**
     * [__construct description]
     * @param Model $model [description]
     */
    public function __construct(
        Model $model
        )
    {
        $this->model = $model;
    }

    /**
     * [details description]
     * @return [type] [description]
     */
    public function details()
    {
        return $this->path;
    }

    /**
     * [pathID description]
     * @return [type] [description]
     */
    public function pathID()
    {
        return $this->path->id;
    }

    /**
     * [name description]
     * @return [type] [description]
     */
    public function name()
    {
        return $this->path->name;
    }

    /**
     * [slug description]
     * @return [type] [description]
     */
    public function slug()
    {
        return $this->path->slug;
    }

    /**
     * [has description]
     * @param  [type]  $pathID [description]
     * @return boolean         [description]
     */
    public function has($pathID)
    {
        if($path = $this->model->findOrFail($pathID)) {
            $this->path = $path;
            return true;
        }

        return false;
    }

    /**
     * [all description]
     * @return [type] [description]
     */
    public function all()
    {
        return $this->model->with('options')->paginate(10);
    }

    /**
     * [create description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function create(Request $request)
    {
        $slug = strtolower(str_replace(' ', '-', $request->input('name')));

        $this->path = $this->model->create([
			'name' => $request->input('name'),
			'description' => $request->input('description'),
			'slug' => $slug,
			'url' => url('paths/' . $slug),
		]);

        return $this->path;
    }

    /**
     * [getDetails description]
     * @param  [type] $pathID [description]
     * @return [type]         [description]
     */
    public function getDetails ($pathID)
    {
		if($this->path = $this->model->with(['options'=> function($q) {
			$q->select('path_id', 'key', 'value', 'section');
		}])->find($pathID)) {

			return $this->path;
		}
		return [];
	}

    /**
     * [update description]
     * @param  [type] $fields [description]
     * @param  string $pathID [description]
     * @return [type]         [description]
     */
    public function update($fields, $pathID = '')
    {
        if($fields && $this->path) {
            foreach($fields as $field => $value) {
                if($field != 'id'){
                    $this->path->$field = $value;
                }
            }
            $this->path->save();

            return $this->path;
        }

        if($fields && $pathID) {
            if($this->has($pathID)) {
                $this->update($fields);
            }
        }

        return false;
    }

    /**
     * [delete description]
     * @param  string $pathID [description]
     * @return [type]         [description]
     */
    public function delete($pathID = '')
    {
        if($this->path) {
            $this->path->delete();
            return true;
        }

        if($pathID) {
            if($this->has($pathID)){
                $this->delete();
            }
        }

        return false;
    }
}
