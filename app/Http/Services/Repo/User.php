<?php

namespace  App\Http\Services\Repo;

use App\User as Model;
use App\Role;

use App\Http\Resources\UserResource;

use App\Events\UsersCreated;
use App\Events\UsersUpdated;

use Illuminate\Http\Request;

class User
{
    /**
     * [protected description]
     * @var [type]
     */
    protected $model;
    protected $role;

    /**
     * [protected description]
     * @var [type]
     */
    protected $user;

    /**
     * [__construct description]
     * @param Model $model [description]
     */
    public function __construct(
        Model $model,
        Role $role
        )
    {
        $this->model = $model;
        $this->role = $role;
    }



    /**
     * [has description]
     * @param  [type]  $pathID [description]
     * @return boolean         [description]
     */
    public function has($userID)
    {
        if($user = $this->model->findOrFail($userID)) {
            $this->user = $user;
            return true;
        }

        return false;
    }

    /**
     * [all description]
     * @return [type] [description]
     */
    public function all()
    {
        return UserResource::collection($this->model->paginate(10));
    }

    /**
     * [create description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function create($request, $password)
    {
        $this->user = $this->model->create([
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'username' => $request->input('username'),
            'email' => $request->input('email'),
            'password' => bcrypt($password),
            'confirmation_token' => str_limit(md5($request->input('email').str_random()), 25, ''),
        ]);

		event(new Created($this->user));

        $this->user->assignRole($this->rolewhere('id', $request->input('role'))->firstOrFail());

        return $this->user;
    }

    /**
     * [getDetails description]
     * @param  [type] $pathID [description]
     * @return [type]         [description]
     */
    public function get ($userID = '')
    {
        if($this->user) return new UserResource($this->user);

        if($userID) {
            if($this->has($userID)) $this->get();
        }
        return $this->user;
	}

    /**
     * [update description]
     * @param  [type] $fields [description]
     * @param  string $pathID [description]
     * @return [type]         [description]
     */
    public function updateRole($role, $userID = '')
    {
        if($role && $this->user) {
            if ($role) $this->user->roles()->sync($role);

            event(new UsersUpdated($this->user));
            return true;
        }

        if($role && $userID) {
            if($this->has($userID)) {
                $this->update($role);
            }
        }

        return false;
    }

    /**
     * [update description]
     * @param  [type] $fields [description]
     * @param  string $pathID [description]
     * @return [type]         [description]
     */
    public function updateStatus($status = '', $userID = '')
    {
        if(is_int($status) && $this->user) {
            $this->user->status = 0;
            if ($status == 0) $this->user->status = 1;

            $this->user->save();

            event(new UsersUpdated($this->user));
        }

        if(is_int($status) && $userID) {
            if($this->has($userID)) {
                $this->updateStatus($status);
            }
        }

        return;
    }

}
