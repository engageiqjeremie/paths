<?php

namespace  App\Http\Services;

use scssc;

use Illuminate\Filesystem\Filesystem;

class Style
{
    /**
     * Traits
     */
    use \App\Http\Traits\PathFile;

    /**
     * [__construct description]
     * @param Filesystem $file [description]
     */
    public function __construct (Filesystem $file)
    {
        $this->file = $file;
    }

    /**
     * [compile description]
     * @param  [type] $scssFolder  [description]
     * @param  [type] $cssFolder   [description]
     * @param  string $formatStyle [description]
     * @return [type]              [description]
     */
    public function compile($scssFolder, $cssFolder, $formatStyle = 'scss_formatter_compressed')
    {
        $compiler = new \scssc();

        $compiler->setImportPaths($scssFolder);
        // set css formatting (normal, nested or minimized), @see http://leafo.net/scssphp/docs/#output_formatting
        $compiler->setFormatter($formatStyle);
        // get all .scss files from scss folder
        $filelist = glob($scssFolder . "*.scss");

        // step through all .scss files in that folder
        foreach ($filelist as $filePath) {
            // get path elements from that file
            $filePathElements = pathinfo($filePath);
            // get file's name without extension
            $fileName = $filePathElements['filename'];
            // Disregard file
            if (substr($fileName, 0, 1) === '_') continue;
            // get .scss's content, put it into $stringSass
            $stringSass = file_get_contents($scssFolder . $fileName . ".scss");
            // compile this SASS code to CSS
            try {
                $stringCss = $compiler->compile($stringSass);

                // write CSS into file with the same filename, but .css extension
                file_put_contents($cssFolder . $fileName . ".css", $stringCss);

                return [
                    'error' => false
                ];
            }
            catch (\Exception $error) {
                return [
                    'error' => true,
                    'msg' => $error->getMessage()
                ];
            }
        }
    }

    /**
     * [size description]
     * @param  [type] $path [description]
     * @return [type]       [description]
     */
    public function size($path)
    {
        return $this->file->size($path);
    }



    public function updateStylesheet($lug, $color, $template, $progressbar)
    {
        // Update stylesheet for compliling
        $this->file->put(
            $this->getResourcePath($lug . '/'. $lug . '.min.scss'),
            // Replace in stub
            $this->getStubAndReplace($color, $template, $progressbar)
        );
    }

    /**
     * [backup description]
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function backup($slug, $color, $template, $progressbar)
    {
        $compiler = new \scssc();

        $resourcePath = $this->getResourcePath($slug);

        $editor = $this->file->get($resourcePath . '/_editor.scss');

        try {
            // $this->pathStyle->updateStylesheet(
            //     $slug,
            //     $color,
            //     $template,
            //     $progressbar
            // );

            $compiler->compile($editor);

            $this->file->put($resourcePath . '/_backup.scss', $editor);
            return [
                'error' => false,
                'content' => $editor,
                'size' =>  $this->file->size($resourcePath . '/_backup.scss')
            ];
        }
        catch (\Exception $error) {
            return [
                'error' => true,
                'msg' => $error->getMessage()
            ];
        }
    }

    /**
     * [backup description]
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function revert($slug)
    {
        $compiler = new \scssc();

        $resourcePath = $this->getResourcePath($slug);

        $editor = $this->file->get($resourcePath . '/_editor.scss');
        $backup = $this->file->get($resourcePath . '/_backup.scss');

        try {
            $compiler->compile($backup);

            $this->file->put($resourcePath . '/_temp.scss', $editor);
            $this->file->put($resourcePath . '/_editor.scss', $backup);
            return [
                'error' => false,
                'content' => $backup,
                'size' =>  $this->file->size($resourcePath . '/_editor.scss'),
                'temp_size' =>  $this->file->size($resourcePath . '/_temp.scss')
            ];
        }
        catch (\Exception $error) {
            return [
                'error' => true,
                'msg' => $error->getMessage()
            ];
        }
    }

    /**
     * [apply description]
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function applyReversion($slug)
    {
        $resourcePath = $this->getResourcePath($slug);

        $this->file->put($resourcePath . '/_temp.scss', '');

        return [
            'error' => false,
            'temp_size' => 0
        ];
    }

    /**
     * [cancelReversion description]
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function cancelReversion($slug)
    {
        $resourcePath = $this->getResourcePath($slug);

        $temp = $this->file->get($resourcePath . '/_temp.scss');

        $this->file->put($resourcePath . '/_temp.scss', '');
        $this->file->put($resourcePath . '/_editor.scss', $temp);

        return [
            'error' => false,
            'content' => $temp,
            'temp_size' =>  0
        ];
    }

    /**
     * [getStubAndReplace description]
     * @param  [type] $color       [description]
     * @param  [type] $template    [description]
     * @param  string $progressbar [description]
     * @return [type]              [description]
     */
    protected function getStubAndReplace ($color, $template, $progressbar = '')
    {
        $stub = $this->file->get($this->getStub('style'));
        $stub = str_replace('dummyColor',$color, $stub);
        $stub = str_replace('dummyTemplate', $template, $stub);
        $stub = str_replace('dummyProgressBar', $progressbar, $stub);

        return $stub;
    }
}
