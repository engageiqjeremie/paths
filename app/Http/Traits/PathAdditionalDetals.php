<?php

namespace App\Http\Traits;

Trait PathAdditionalDetals {

    protected $details = [
        'path_id' => '',
        'pixel' => '',
        'fire_at' => '',
        'revenue_tracker_id' => '',
        'filters' => []

    ];

    protected function processPathAdditionalDetals($details)
    {
        if(is_object($details)) $this->details = array_replace($this->details, collect($details)->toArray());

        $filters = [];
        $icons = [];
        if(array_key_exists('filters', $this->details)) {
            $questions = $this->details['filters'];
            foreach($questions as $question) {
                $filters[$question->id] = $question->name;
                $icons[$question->id] = $question->image;
            }
        }

        return [
            $this->details,
            $filters,
            $icons
        ];
    }

}
