<?php

namespace App\Http\Traits;

/**
 * Files and folders related to path
 *
 */
Trait PathFile
{
    /**
     * @param string|null $fileName
     *
     * @throws \Sven\ArtisanView\Exceptions\UnsupportedException
     *
     * @return string
     */
    public function getResourcePath($fileName = null)
    {

        $path = resource_path() . '/assets/sass/paths';

        return $this->normalizePath($path . DIRECTORY_SEPARATOR . $fileName);
    }

    /**
     * @param string|null $fileName
     *
     * @throws \Sven\ArtisanView\Exceptions\UnsupportedException
     *
     * @return string
     */
    public function getPublicPath ($fileName = null)
    {

        $path = public_path() . '/css/paths';

        return $this->normalizePath($path . DIRECTORY_SEPARATOR . $fileName);
    }
    /**
     * @param string|null $fileName
     *
     * @throws \Sven\ArtisanView\Exceptions\UnsupportedException
     *
     * @return string
     */
    public function getPath($domain, $fileName = null)
    {
        if($domain == 'controller') $path = $this->controllerPath();
        if($domain == 'view') $path = $this->viewPath();
        if($domain == 'route') $path = $this->routePath();
        if($domain == 'storage') $path = $this->storagePath();

        return $this->normalizePath($path . DIRECTORY_SEPARATOR . $fileName);
    }


    /**
     * @param string|null $fileName
     *
     * @throws \Sven\ArtisanView\Exceptions\UnsupportedException
     *
     * @return string
     */
    protected function controllerPath()
    {
        return base_path() . '/app/Http/Controllers/' . ucwords($this->getEnvironmentFolderName());
    }

    /**
     * [viewPath description]
     * 
     * @return [type] [description]
     */
    protected function viewPath ()
    {
        /** @var \Illuminate\View\FileViewFinder $viewFinder */
        $viewFinder = app('view.finder');

        $paths = $viewFinder->getPaths();

        // If we have more than one path configured, throw an
        // exception as this is currently not supported by
        // the package. It might be supported later on.
        if (count($paths) !== 1) {
            //throw UnsupportedException::tooManyPaths(count($paths));
        }

        return reset($paths);
    }

    /**
     * @param string|null $fileName
     *
     * @throws \Sven\ArtisanView\Exceptions\UnsupportedException
     *
     * @return string
     */
    protected function routePath()
    {
        return base_path() . '/routes/' . strtolower($this->getEnvironmentFolderName());
    }

    /**
     * @param string|null $fileName
     *
     * @throws \Sven\ArtisanView\Exceptions\UnsupportedException
     *
     * @return string
     */
    protected function storagePath()
    {
        return storage_path() . '/paths';
    }

    /**
     * [getStub description]
     * @param  [type] $path [description]
     * @return [type]       [description]
     */
    public function getStub($domain, $path = '')
    {
        if($domain == 'controller') return $this->controllerStub();
        if($domain == 'view') return $this->viewStub($path);
        if($domain == 'route') return $this->routeStub($path);
        if($domain == 'style') return $this->styleStub();
    }

    /**
     * @param string $path
     *
     * @return string
     */
    public function normalizePath($path)
    {
        $withoutBackslashes = str_replace('\\', DIRECTORY_SEPARATOR, $path);

        return str_replace('/', DIRECTORY_SEPARATOR, $withoutBackslashes);
    }

    /**
     * [getStub description]
     * @param  [type] $path [description]
     * @return [type]       [description]
     */
    protected function controllerStub()
    {
        return base_path() . '/app/Http/Controllers/Stubs/Controller.custom.stub';
    }

    /**
     * [viewStub description]
     * @param  [type] $path [description]
     * @return [type]       [description]
     */
    public function viewStub ($path)
    {
        return $path . '/stubs/path.custom.stub';
    }

    /**
     * [getStub description]
     * @param  [type] $path [description]
     * @return [type]       [description]
     */
    protected function routeStub($path)
    {
        return $path . '/path-route.custom.stub';
    }

    /**
     *
     *
     * @return string
     */
    public function styleStub()
    {
        $path = resource_path() . '/assets/sass/stubs/path-style.custom.stub';

        return $this->normalizePath($path);
    }

    /**
     * @param string|null $fileName
     *
     * @throws \Sven\ArtisanView\Exceptions\UnsupportedException
     *
     * @return string
     */
    protected function routeStubPath()
    {
        $path = base_path() . '/routes/stubs';

        return $this->normalizePath($path);
    }

    /**
     * @param string $path
     */
    protected function createFolderIfNotExists($folderPath)
    {
        if (! is_dir($folderPath)) {
            mkdir($folderPath, 0777, true);
        }
    }

    /**
    * Get folder name
    *
    * @return string
    */
    protected function getEnvironmentFolderName ()
    {
        if(app('env') == 'production') return 'paths';
        return 'devpaths';
    }
}
