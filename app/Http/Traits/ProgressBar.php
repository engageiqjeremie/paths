<?php

namespace App\Http\Traits;

Trait ProgressBar
{
    protected $progress = 0;

    protected $portion = [
        'landing' => 1/4,
        'registration' => 1/2,
        'question' => 3/4
    ];

    public function progress()
    {
        return $this->progress;
    }

    protected function setProgress($hasCampaign, $campaigns, $pageName, $page = 1)
    {
        if(array_key_exists($pageName, $this->portion)) $portion = $this->portion[$pageName];
        else $portion = 1;

        if($hasCampaign) $this->progress = (((int) $page / count($campaigns)) * 100) * $portion;
        else $this->progress = (((int) $page / 12) * 100) * $portion;
    }
}
