<?php

namespace App\Listeners\Users;

use App\Events\BugsCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;

class BugCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BugsCreated  $event
     * @return void
     */
    public function handle(BugsCreated $event)
    {
        $event->bug->created_by = Auth::user()->name;
        $event->bug->save();
    }
}
