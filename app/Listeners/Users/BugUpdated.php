<?php

namespace App\Listeners\Users;

use App\Events\BugsUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;

class BugUpdated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BugsUpdated  $event
     * @return void
     */
    public function handle(BugsUpdated $event)
    {
        $event->bug->updated_by = Auth::user()->name;
        $event->bug->save();
    }
}
