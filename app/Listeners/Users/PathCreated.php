<?php

namespace App\Listeners\Users;

use App\Events\PathsCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;
class PathCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PathsCreated  $event
     * @return void
     */
    public function handle(PathsCreated $event)
    {
        $event->path->created_by = Auth::user()->name;
        $event->path->save();
    }
}
