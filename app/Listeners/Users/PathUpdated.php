<?php

namespace App\Listeners\Users;

use App\Events\PathsUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;

class PathUpdated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PathsUpdated  $event
     * @return void
     */
    public function handle(PathsUpdated $event)
    {
        $event->path->updated_by = Auth::user()->name;
        $event->path->save();
    }
}
