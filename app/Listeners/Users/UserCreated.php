<?php

namespace App\Listeners\Users;

use App\Events\UsersCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;

class UserCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserCreated  $event
     * @return void
     */
    public function handle(UsersCreated $event)
    {
        $event->user->created_by = Auth::user()->name;
        $event->user->save();
    }
}
