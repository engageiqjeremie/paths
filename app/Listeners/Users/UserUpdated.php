<?php

namespace App\Listeners\Users;

use App\Events\UsersUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;

class UserUpdated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserUpdated  $event
     * @return void
     */
    public function handle(UsersUpdated $event)
    {
        $event->user->updated_by = Auth::user()->name;
        $event->user->save();
    }
}
