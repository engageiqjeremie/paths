<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $fillable = [
        'path_id',
        'key',
        'value',
        'section'
    ];


    // public function path()
    // {
    //     return $this->belongsTo('App\Path');
    // }
}
