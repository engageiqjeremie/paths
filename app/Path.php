<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Path extends Model
{
    protected $fillable = [
        'name',
        'comments',
        'description',
        'slug',
        'url',
        'created_by',
        'updated_by'
    ];

    public function options()
    {
        return $this->hasMany('App\Option');
    }

    // Delete cascade
    protected static function boot(){
        parent::boot();

        static::deleting(function($path)
        {
            $path->options()->delete();
        });
    }
}
