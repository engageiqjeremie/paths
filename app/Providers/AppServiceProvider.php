<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Resources\Json\Resource;
use Laravel\Dusk\DuskServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Resource::withoutWrapping();
        Validator::extend('engageiq_email', function($attribute, $value, $parameters, $validator) {
            if(preg_match('|@engageiq.com$|', $value)) {
                return $value;
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // $files = $this->app['files']->files(base_path() . '/test');
        //
        //  foreach($files as $file)
        // {
        //     $config = $this->app['files']->getRequire($file);
        //     //
        //     // special case for files named config.php (config keyword is omitted)
        //     if($this->app['files']->name($file) === 'config')
        //     {
        //             $this->app['config']->set('path' , $config);
        //     }
        // }
        // echo config('leadreactor.lead_reactor_url') . '<br />';
        // echo config('path.awww2.awww1') . '<br />';

        if ($this->app->environment('local', 'testing')) {
            $this->app->register(DuskServiceProvider::class);
        }
    }
}
