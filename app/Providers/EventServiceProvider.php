<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],
        'App\Events\BugsCreated' => [
            'App\Listeners\Users\BugCreated',
        ],
        'App\Events\BugsUpdated' => [
            'App\Listeners\Users\BugUpdated',
        ],
        'App\Events\PathsCreated' => [
            'App\Listeners\Users\PathCreated',
        ],
        'App\Events\PathsUpdated' => [
            'App\Listeners\Users\PathUpdated',
        ],
        'App\Events\CommentsCreated' => [
            'App\Listeners\Users\CommentCreated',
        ],
        'App\Events\UsersCreated' => [
            'App\Listeners\Users\UserCreated',
        ],
        'App\Events\UsersUpdated' => [
            'App\Listeners\Users\UserUpdated',
        ],
        'Illuminate\Auth\Events\Login' => [
            'App\Listeners\Users\UpdateLastLoggedIn',
        ],
        'Illuminate\Auth\Events\Logout' => [
            'App\Listeners\Users\UpdateLastLoggedOut',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
