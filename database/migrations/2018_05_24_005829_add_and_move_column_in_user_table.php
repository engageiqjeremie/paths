<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAndMoveColumnInUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('name', 'first_name');
            $table->string('last_name');
            $table->string('username');
            $table->string('address', 255)->nullable();
            $table->string('skills', 255)->nullable();
            $table->string('department')->nullable();
            $table->longText('personal_information')->nullable();
        });

        // if(config('database.default') == 'mysql') $this->moveColumn();

        $this->alter();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(
                [
                    'first_name',
                    'last_name',
                    'username',
                    'username',
                    'address',
                    'skills',
                    'department',
                    'personal_information',
                ]
            );
        });
    }

    protected function moveColumn ()
    {
        \DB::statement('ALTER TABLE users MODIFY COLUMN last_name varchar(191) AFTER first_name');
        \DB::statement('ALTER TABLE users MODIFY COLUMN username varchar(191) AFTER last_name');
        \DB::statement('ALTER TABLE users MODIFY COLUMN email varchar(191) AFTER username');
        \DB::statement('ALTER TABLE users MODIFY COLUMN password varchar(191) AFTER email');
        \DB::statement('ALTER TABLE users MODIFY COLUMN address varchar(255) AFTER password');
        \DB::statement('ALTER TABLE users MODIFY COLUMN role varchar(191) AFTER address');
        \DB::statement('ALTER TABLE users MODIFY COLUMN skills varchar(255) AFTER role');
        \DB::statement('ALTER TABLE users MODIFY COLUMN department varchar(191) AFTER skill');
        \DB::statement('ALTER TABLE users MODIFY COLUMN personal_information longtext AFTER department');
        \DB::statement('ALTER TABLE users MODIFY COLUMN avatar text AFTER personal_information');
        \DB::statement('ALTER TABLE users MODIFY COLUMN status tinyint(1) AFTER avatar');
        \DB::statement('ALTER TABLE users MODIFY COLUMN confirmed tinyint(1) AFTER status');
        \DB::statement('ALTER TABLE users MODIFY COLUMN confirmation_token varchar(25) AFTER confirmed');
        \DB::statement('ALTER TABLE users MODIFY COLUMN api_token varchar(60) AFTER confirmation_token');
        \DB::statement('ALTER TABLE users MODIFY COLUMN remember_token varchar(100) AFTER api_token');
    }

    protected function alter()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('first_name')->nullable()->change();
            $table->string('last_name')->nullable()->change();
            $table->string('username')->nullable(false)->change();
            $table->string('email')->nullable(false)->change();
            $table->text('avatar')->nullable()->change();
            $table->string('role')->nullable()->change();
            $table->boolean('status')->default(0)->change();
            $table->boolean('confirmed')->default(0)->change();
            $table->string('confirmation_token', 25)->nullable()->change();
            $table->string('api_token', 60)->nullable()->change();
            $table->string('password')->change();
            $table->string('address', 255)->nullable()->change();
            $table->string('skills', 255)->after('address')->nullable()->change();
            $table->string('department')->after('skill')->nullable()->change();
            $table->longText('personal_information')->nullable()->change();
        });
    }
}
