<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()['cache']->forget('spatie.permission.cache');

        // create permissions
        Permission::create(['name' => 'Administer Paths']);
        Permission::create(['name' => 'Edit Path']);
        Permission::create(['name' => 'Create Path']);
        Permission::create(['name' => 'Administer roles & permissions']);
        Permission::create(['name' => 'Delete Path']);
        Permission::create(['name' => 'Administer Bugs']);
        Permission::create(['name' => 'Create Bug']);
        Permission::create(['name' => 'Edit Bug']);
        Permission::create(['name' => 'Delete Bug']);
        Permission::create(['name' => 'Administer Documentation']);
        // create roles and assign existing permissions
        $role = Role::create(['name' => 'Site Administrator']);
        //$role->givePermissionTo('edit articles');
    }
}
