
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import './bootstrap.js';
import './package.js';
import './component.js';
import './prototype.js';


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import router from './router';
import store from './store';

import customheader from './partials/Header.vue';
import sidebar from './partials/Sidebar.vue';
import appbody from './partials/Appbody.vue';
import appfooter from './partials/Footer.vue';

const app = new Vue({
    el: '#dashboard',
    components : {
        customheader,
        sidebar,
        appbody,
        appfooter,
    },
    store: store,
    created () {
        this.fetchallUser();
        this.getUserRoles();
    },
    methods: {
        fetchallUser() {
            axios.get('/api/all-users')
            .then((response) => {
                let users = response['data'].data;

                if(!users) return;

                for(let user of users) {
                    this.$store.commit('USER/ADD', user);
                }
                // console.log('username: ', App.username);
                this.$store.commit('USER/SET_LOGGEDIN_USER', App.username);

            })
            .catch((err) => {
                console.log(err.toString())
            })
        },
        getUserRoles() {
            axios.get('/api/get-user-roles')
            .then((roles) => {
                this.$store.commit('USER/ROLES', roles.data);
            })
            .catch((err) => {
                console.log(err.toString());
            })
        },
    },
    router
});
