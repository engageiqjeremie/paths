/**********************************************************
Spinner
**********************************************************/
import Spinners from './components/Util/spinners';
Vue.component('spinner', Spinners);

/**********************************************************
Editor
**********************************************************/
import Editor from './components/Util/Wysiwyg.vue';
Vue.component("wysiwyg", Editor);

/**********************************************************
Paginator
**********************************************************/
import Pagination from './components/Util/Paginator.vue';
Vue.component("pagination", Pagination);
