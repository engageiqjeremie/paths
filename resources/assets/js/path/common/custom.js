function checkIfBrowserIE() {
	var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer, return version number
        // alert(parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))));
    	return true;
    else                 // If another browser, return 0
    	return false;
        //alert('otherbrowser');
}

function sendForm() {
	var form = $('.survey_form');

	setPhoneAndAddress(form);

	$.ajax({
		url: form.attr('action'),
		dataType: 'jsonp',
		data: form.serialize(),
		success: function(data) {
			console.log(data);
		},
		error: function (jqXHR, text, errorThrown) {
		    console.log(jqXHR + " " + text + " " + errorThrown);
		}
	});
}

function manualSendForm(form) {

	setPhoneAndAddress(form);

	$.ajax({
		url: form.attr('action'),
		dataType: 'jsonp',
		data: form.serialize(),
		success: function(data) {
			console.log(data);
		},
		error: function (jqXHR, text, errorThrown) {
		    console.log(jqXHR + " " + text + " " + errorThrown);
		}
	});
}
// not called
function setNextSurvey(id, callback) {
	$.ajax({
		url:"paths/session",
		type: 'POST',
		data: {
			type : 'set_next_survey',
			id : id
		},
		success: function(next_campaign_priority) {
			callback(next_campaign_priority);
		}
	});
}

function loadSurvey(id) {
	var lrUrl = $("meta[name='lrUrl']").attr('content');
	$.ajax({
		url: lrUrl + "get_campaign_content",
		dataType: 'jsonp',
		data: {
			'id' : id
		},
		success:function(json){
			$.ajax({
				url:"paths/session",
				type: 'POST',
				data: {
					type : 'convert_html',
					html : json.content
				},
				success: function(converted_html) {
					//$('#contentbox').html(json.content);
					$( "#progress_bar_row td.cell_noshade:first" ).removeClass('cell_noshade').addClass('cell_shade');
					var cur_num = parseInt($('#progress_bar_current_number').html()) + 1;
					$('#progress_bar_current_number').html(cur_num);
					$('#contentbox').html(converted_html);

					if(converted_html.indexOf('<script>') > 0)
	                {
	                    //location.reload();
	                    $("#contentbox").find("script").each(function(i) {
	                        eval($(this).text());
	                    });
	                }
					//console.log(converted_html);
					console.log('loadSurvey');
				}
			});
		},
		error:function(){
		 alert("Error");
		}
	});

}

function convertHtml(html,callback) {
	$.ajax({
		url:"paths/session",
		type: 'POST',
		data: {
			type : 'convert_html',
			html : html
		},
		success: function(converted_html) {
			callback(converted_html);
		}
	});
}

function setNextStackSet() {
	window.location.replace($('#campaign_url').val() + $('#next_stack').val());
}

function setPhoneAndAddress(form) {
	//Checks if form has phone input

	if($('#user_phone').val() != '' && $('#user_address').val() != '') {
		return;
	}

	if(form.find('input[name="phone"]').length == 1 && $('#user_phone').val() == '') {
	// if(form.find('input[name="phone"]').length == 1) {

		var phone = form.find('input[name="phone"]').val();
		phone = phone.replace(/\D/g,'');

		if(phone) {
			$.ajax({
				url: '/paths/session/store',
				type: 'GET',
				data: {phone: phone},
				success: function(data) {
					$('#user_phone').val(phone)
				}
			});
		}
	}

	//Checks if form has address input
	if((form.find('input[name="address"]').length == 1 || form.find('input[name="address1"]').length == 1)&& $('#user_address').val() == '') {
	// if(form.find('input[name="address"]').length == 1 || form.find('input[name="address1"]').length == 1) {

		if(form.find('input[name="address1"]').length == 1) var address = form.find('input[name="address1"]').val();
		else var address = form.find('input[name="address"]').val();

		if(address) {
			$.ajax({
				url: '/paths/session/store',
				type: 'GET',
				data: {address: address},
				success: function(data) {
					$('#user_address').val(address);
				}
			});
		}
	}
}

function popupwindow(url, title, w, h) {
  var left = (screen.width/2)-(w/2);
  var top = (screen.height/2)-(h/2);
   window.open(url, title, "directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width="+w+", height="+h+", top="+top+", left="+left);
   return false;
}


$.validator.messages.required = "Required!";

$.validator.addMethod("letterswithspace", function(value, element) {
	return this.optional(element) || /^[a-z\s]+$/i.test(value);
}, "letters only");


$.validator.addMethod("email2", function(value, element, param) {
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  	return emailReg.test( value );
}, $.validator.messages.email);

$.validator.addMethod("zipcode", function(value, element) {
  return this.optional(element) || /^\d{5}(?:-\d{4})?$/.test(value);
}, "Please provide a valid zipcode.");

var firstErrorInput = '';

$.validator.setDefaults({
	focusInvalid: false,
	invalidHandler: function(form, validator) {
		if (!validator.numberOfInvalids())
			return;
		console.log('validator runs');
		if (!firstErrorInput) {
			firstErrorInput = validator.errorList[0].element;
		}
		var errors = validator.numberOfInvalids();
		if (errors) {
			$('html, body').stop().animate({
				scrollTop: $(firstErrorInput).offset().top - 100
			}, 1000, function(){
				console.log(firstErrorInput);
				$('html, body').stop();
				// SUBMIT_BTN.enable();
				$(firstErrorInput).focus();
				firstErrorInput = '';
			});
		}
	},
});

//Script to display additional input fields in Desktop
function showform()
{
    $( "#formtable" ).css("display", "block" );
    $( ".yes-no-desktop" ).css("display", "none" );
}

//Script to hide additional input fields for Desktop
function hideform()
{
    $( "#formtable" ).css("display", "none" );
    $( ".yes-no-desktop" ).css("display", "block" );
}

//Script to hide additional input fields in Mobile
function backquestion()
{
    $('.yes-no-mobile').fadeIn(500);
    $('#formtable').fadeOut(500);
    $('html, body').animate({
        scrollTop: $(".wrapper").offset().top
    }, 500);
}

$(document).ready(function() {

	//Script to display additional input fields in Mobile
	$(".yes-no-mobile .class-yes").click(function(){
	    $('.yes-no-mobile').fadeOut(500);
	    $('#formtable').fadeIn(500);
	    $('html, body').animate({
	        scrollTop: $("#formtable").offset().top
	    }, 500);
	});

	var lrUrl = $("meta[name='lrUrl']").attr('content');

	var error_validation_counter = 0;


	$(document).on('submit','.survey_form',function(e)
    {
		e.preventDefault();

		$('.submit_form').attr('disabled',true).prop('disabled', true).attr('type','button').css('background-image','url(images/yes-loading.gif)');
		$('.click-submit').attr('disabled',true).prop('disabled', true).attr('type','button').css('background-image','url(images/yes-loading.gif)');
		sendForm();

		var id = $('.survey_form input[name="eiq_campaign_id"]').val();
		console.log(id);
		setNextSurvey(id, function(next_campaign) {
			// loadSurvey(next_campaign);
			History.pushState({state:next_campaign}, null, "?campaign="+next_campaign);
			var redUrl = window.location.origin + window.location.pathname + '?campaign=' + next_campaign;
			console.log(redUrl);
			window.location.replace(redUrl);
			//location.reload();
		});

	});

	// $(document).on('click','.submit_form',function(e)
 //    {
	// 	e.preventDefault();
	// 	//$('#contentbox').html('<div align="center"><img src="images/icon_loading.gif" /><h2>Loading Surveys...</h2></div>');

	// 	$('.survey_form').submit();
	// });

	$(document).on('click','.next_survey',function(e)
    {
		e.preventDefault();

		$(this).attr('disabled',true).prop('disabled', true).css('background-image','url(images/no-loading.gif)');

		var id = $('.survey_form input[name="eiq_campaign_id"]').val();
		console.log(id);
		setNextSurvey(id, function(next_campaign) {
			//loadSurvey(next_campaign);
			//location.reload();
			History.pushState({state:next_campaign}, null, "?campaign="+next_campaign);
			var redUrl = window.location.origin + window.location.pathname + '?campaign=' + next_campaign;
			window.location.replace(redUrl);
		});
	});

	$(document).on('click','.pop_up',function(e)
    {
		e.preventDefault();
		var url = $(this).data('url');
		window.open(url, '_blank', 'scrollbars=1,height=800,width=1024,left=1,top=1,resizable=1');

		var id = $('.survey_form input[name="eiq_campaign_id"]').val();
		console.log(id);
		setNextSurvey(id, function(next_campaign) {
			History.pushState({state:next_campaign}, null, "?campaign="+next_campaign);
			var redUrl = window.location.origin + window.location.pathname + '?campaign=' + next_campaign;
			window.location.replace(redUrl);
			//loadSurvey(next_campaign);
		});
	});

	$(document).on('click','.pop_up_redirect',function()
    {
		var url = $(this).data('url');
		window.open(url, '_blank', 'scrollbars=1,height=800,width=1024,left=1,top=1,resizable=1');
	});

	$(document).on('click','#submit_stack_button',function(e)
    {
    	e.preventDefault();

    	var this_button = $(this);
    	this_button.attr('disabled',true).prop('disabled', true);

		var total_yes = $('.submit_stack_campaign:checked').length;
		var validation_counter = 0;
		var sent_counter = 0;
		 $('#error_validation_counter').val( 0 );

		// console.log( $('.submit_stack_campaign:checked').length );
	    if($('.submit_stack_campaign:checked').length == 0) {
	    	console.log('No YES');
	    	setNextStackSet();
	    }else {
			$("form").each(function () {
			    var form = $(this);
			    if(form.find('.submit_stack_campaign').is(':checked')) {
			    	if(form.valid()) {
			    		validation_counter++;
			    	}
			    }
			});

			if(total_yes == validation_counter) {
				this_button.html('Sending');
				$("form").each(function () {
					var form = $(this);
					if(form.find('.submit_stack_campaign').is(':checked')) {
						if(form.valid()) {
							if(typeof form.attr('data-valid') === 'undefined' || form.data('valid') == 'true') {
								setPhoneAndAddress(form);
								form.submit();
								console.log('SUBMIT');
							}
				    	}
					}
				});
				console.log('Next Stack');
				setNextStackSet();
			}else {
				console.log('Not Submit');
				$(this).attr('disabled',false);
			}
	    }



	});

	$(document).on('submit','.stack_survey_form',function(e)
    {
    	e.preventDefault();

    	// var submit_button = $('#submit_stack_button');
    	// submit_button.css('background-image','url("images/loading-button.gif")');

		var form = $(this);

		setPhoneAndAddress(form);

		console.log(form.attr('id'));
		if(form.attr('action') != '') {
			$.ajax({
				url: form.attr('action'),
				dataType: 'jsonp',
				data: form.serialize(),
				error: function (jqXHR, text, errorThrown) {
					// submit_button.css('background-image','url("images/submit_button_form.png")');
				    console.log(jqXHR + " " + text + " " + errorThrown);
				    form.attr('data-sent','true').data('sent','true');
				},
				success: function(data) {
					console.log(data);
					// submit_button.css('background-image','url("images/submit_button_form.png")');
					console.log('SUCCESS');
					form.attr('data-sent','true').data('sent','true');
				}
			});
			console.log('SENT');
		}else {
			console.log('SKIP');
		}

	});

	$(document).on('click','.show_custom_questions',function(e)
	{
	  var form = $(this).closest('form');
	  form.find('#custom_questions').css("display", "block" );
	});

	$(document).on('click','.hide_custom_questions',function(e)
	{
	  var form = $(this).closest('form');
	  form.find('#custom_questions').css("display", "none" );
	});

	$(document).on('click','.pop_up_stack',function()
    {
    	var url = $(this).data('url');
		window.open(url, '_blank', 'scrollbars=1,height=800,width=1024,left=1,top=1,resizable=1');
	});

	$(document).on('change','input[name="phone"]',function()
    {
    	var phone = $(this).val();
    	// phone = phone.replace(/\D/g,'');
    	$('input[name="phone"]').val(phone);
	});

	$(document).on('change','input[name="address"]',function()
    {
    	var address = $(this).val();
    	$('input[name="address"]').val(address);
	});

	$("input[name=phone]").mask("(999) 999-9999");

    $(".act-more").click(function(){
		$(".more-content").slideToggle(300);
		$('html, body').animate({
			scrollTop: $(".more-content").offset().top
		}, 500);
	});

	$(".read-more-btn").click(function(){
		var the_content = $(this).closest('.read-more-div').find(".read-more-cnt");
		the_content.slideToggle(300);
		$('html, body').animate({
			scrollTop: the_content.offset().top
		}, 500);
	});
});
