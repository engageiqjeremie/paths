/**********************************************************
Authorization
**********************************************************/
let authorizations = require('./util/authorizations');

Vue.prototype.authorize = function (...params) {
    if (! App.signedIn) return false;

    if (typeof params[0] === 'string') {
        return authorizations[params[0]](params[1]);
    }

    return params[0](App.user);
};

/**********************************************************
Capitalize
**********************************************************/
Vue.prototype.capitalize = function(str)
{
    if(!str) return '';

    str = str.replace('_', ' ');
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

/**********************************************************
Moment
**********************************************************/
Vue.prototype.ago = (date) =>
{
    return moment(date).fromNow();
}

/**********************************************************
Role
**********************************************************/
 Vue.prototype.isSystemAdmin = () =>
 {
    if (window.App.role === 'Site Administrator') {
        return true;
    }
    return false;
 };

 Vue.prototype.extend = ($default, $input) =>
 {
     // Go through default object and iterate
     // We used the default object for it has the complete parameters
     for (var property in $input) {
         // Check $input object if the property of defaut exists on its own
         if ($input.hasOwnProperty(property)) {
             // Detemine if the $input property is object
             if(typeof($input[property]) === 'object') {
                 // go through the $default property
                 // We used the default object property for it has the complete parameters
                 for (var inner_property in $input[property]) {
                     // Check $input object if the property of defaut exists on its own
                     if ($input[property].hasOwnProperty(inner_property)) {
                         // Replace the default property value with $input property value
                         $default[property][inner_property] = $input[property][inner_property];
                     }
                 }
             }
             // $input property is not object
             else {
                 // Replace the default property value with $input property value
                 $default[property] = $input[property];
             }
         }
     }

     return $default;
 }
