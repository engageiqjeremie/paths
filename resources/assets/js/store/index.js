import Vuex from 'vuex';

import userModdule from './modules/user';
import pathModdule from './modules/path';
import templateModdule from './modules/template';
import trackerModdule from './modules/tracker';
import progressbarModdule from './modules/progressbar';
import emailModdule from './modules/email';


export default new Vuex.Store({
    modules: {
        USER: userModdule,
        PATH: pathModdule,
        TEMPLATE: templateModdule,
        TRACKER: trackerModdule,
        PROGRESSBAR: progressbarModdule,
        EMAIL: emailModdule
    }
})
