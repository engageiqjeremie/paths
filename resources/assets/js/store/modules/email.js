const namespaced = true;

const state = {
	all: [],
}

const getters = {
	emails_count: (state) => state.all.length
}

const mutations = {
	ADD: (state, $email) => {
        state.all.unshift($email);
    }
}

const actions = {
	addEmail (context, value) {
		context.commit('ADD', value)
	},
}

const module = {
	namespaced,
    state,
    getters,
    mutations,
    actions
};

export default module;
