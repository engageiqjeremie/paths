const namespaced = true;

const state = {
	all: [],
	current: [],
	slug: '',
	template_slug: '',
	preview_src: '',
	has_selected_path: false,
	show_list: true,
	show_create_form: false
}

const getters = {
}

const mutations = {
	ADD: (state, $paths) => {
        state.all.unshift($paths);
    },
	REMOVE: (state, $path_id) => {
        state.all = _.remove(state.all, function($path) {
		  return $path.id != $path_id;
		});
    },
	UPDATE_SLUG: (state, $slug) => {
		console.log(state.current);
		state.slug = $slug;
		if(!$slug) {
			state.current = [];
			state.template_slug = '';
			state.has_selected_path = false;
			return;
		}
		state.current = _.find(state.all, (path) => path.slug == $slug);
		if(state.current.hasOwnProperty('options')) {
			console.log('che: ', state.current);
			state.template_slug = (state.current.options.hasOwnProperty('themes')) ? state.current.options.themes.template : 'default';
		}
		state.preview_src = state.current.url;
		state.has_selected_path = true;
	},
    UPDATE_PATH_BY_INDEX: (state, $payload) => {
        state.current[$payload.key] = $payload.value;
    },
	PREVIEW_SRC: (state, $payload) => {
        state.preview_src = '';
		setTimeout(function(){
			state.preview_src = state.current.url;
		}, 2000);
    },
	UPDATE_STATUS: (state, $payload) => {
		let path =  _.find(state.all, (path) => path.id == $payload.id);

		if (path) {
		    let index = state.all.indexOf(path);
		    state.all[index].status = $payload.status;
		    state.all[index].updated_by = $payload.updated_by;
		    state.all[index].date_updated = moment($payload.date_updated).fromNow();

			if(state.has_selected_path) {
				path.status = $payload.status;
			    path.updated_by = $payload.updated_by;
			    path.date_updated = moment($payload.date_updated).fromNow();

				state.current = path;
			}
			return true;
		}
		return false;
	},
	TOGGLEFORM: (state, $payload) => {
        state.show_list = !state.show_list;
        state.show_create_form = !state.show_create_form;
    }
}

const actions = {
	addPath (context, value) {
		context.commit('ADD', value)
	},
	removePath (context, value) {
		context.commit('REMOVE', value)
	},
	setPathSlug (context, value) {
		context.commit('UPDATE_SLUG', value)
	},
	updatePathByIndex (context, value) {
		context.commit('UPDATE_PATH_BY_INDEX', value)
	},
	reloadPreviewSrc (context, value) {
		context.commit('PREVIEW_SRC', value)
	},
	update_status (context, payload) {
		return context.commit('UPDATE_STATUS', payload);
	},
	toggleForm (context, payload) {
		context.commit('TOGGLEFORM', payload);
	}
}

const module = {
	namespaced,
    state,
    getters,
    mutations,
    actions
};

export default module;
