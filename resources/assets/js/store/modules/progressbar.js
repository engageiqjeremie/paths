const namespaced = true;



const state = {
	all: []
}

const getters = {
}

const mutations = {
	ADD: (state, $trackers) => {
        state.all = $trackers;
    },
}

const actions = {
	addProgressbar (context, value) {
		context.commit('ADD', value)
	}
}

const module = {
	namespaced,
    state,
    getters,
    mutations,
    actions
};

export default module;
