const namespaced = true;

const default_template = {
	option_inputs: []
}

const state = {
	all: [],
	current: [],
	option_inputs: [],
	slug: '',
	preview_slug: '',
	has_option_inputs: false
}

const getters = {
}

const mutations = {
	ADD: (state, $templates) => {
        state.all = $templates;
    },
	SLUG: (state, $slug) => {
        state.slug = $slug;
		if(!$slug) {
			state.current = [];
			state.option_inputs = [];
			state.has_option_inputs = false;
			return;
		}
		state.current = state.all[$slug];
		state.option_inputs = state.current.option_inputs;
		state.has_option_inputs = true;
    },
	PREVIEW_SLUG: (state, $preview_slug) => {
        state.preview_slug = $preview_slug;
    },
}

const actions = {
	addTemplates (context, value) {
		context.commit('ADD', value)
	},
	setTemplateSlug (context, value) {
		context.commit('SLUG', value)
	},
	setPreviewSlug (context, value) {
		context.commit('PREVIEW_SLUG', value)
	},
}

const module = {
	namespaced,
    state,
    getters,
    mutations,
    actions
};

export default module;
