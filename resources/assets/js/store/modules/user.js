const namespaced = true;

const default_user_data = {
	first_name: '',
	last_name: '',
	username: '',
	email: '',
	role: '',
	date_updated: '',
	last_loggedin: 'Offline',
	avatar: '/img/default/profile.jpg',
	roles: []
}

const state = {
	all: [],
    id: null,
    current: default_user_data,
    loggedin_user: {
        id: '',
        email: '',
        username: '',
        roles: ''
    },
	has_current: false,
	show_create_form:false,
	show_list: true
}

const getters = {
	users: (state) => state.all,
    user: (state) => state.current,
	is_loggedin: (state) => state.current.last_loggedin
}

const mutations = {
	ADD: (state, $users) => {
        // console.log($users);
        // state.users[payload.id] = $users;
        state.all.unshift($users);
        // state.users = $users;
    },
    SET_LOGGEDIN_USER: (state, $username) => {
        state.loggedin_user = _.find(state.all, function(user) { return user.username == $username; });
    },
    SET_CURRENT_USER: (state, $id) => {
		if(!$id) {
			state.id = '';
	        state.current = default_user_data;
			state.has_current = false;
			return;
		}
        state.id = $id;
        state.current = _.find(state.all, function(user) { return user.id == $id; });
		state.has_current = true;
    },
    UPDATE_USER_BY_INDEX: (state, $payload) => {
		if(!state.id) {
			let index =_.findIndex(state.all, function(user) { return user.id == $payload.id; });
			state.all[index][$payload.key] = $payload.value;
			return;
		}

        state.current[$payload.key] = $payload.value;
    },
    ROLES: (state, $roles) => {
        state.roles = $roles;
    },
	TOGGLEFORM: (state, $payload) => {
        state.show_list = !state.show_list;
        state.show_create_form = !state.show_create_form;
    }
}

const actions = {
	add (context, value) {
		context.commit('ADD', value)
	},
	updateUserByIndex (context, value) {
		context.commit('UPDATE_USER_BY_INDEX', value)
	},
	setLoggedinUser (context, value) {
		context.commit('SET_LOGGEDIN_USER', value)
	},
	setUser (context, value) {
		// console.log(context.state.id, context.state.all.length);
		// if(!context.state.id && context.state.all.length)
		 context.commit('SET_CURRENT_USER', value)
	},
	toggleForm (context, payload) {
		context.commit('TOGGLEFORM', payload);
	}
}

const module = {
	namespaced,
    state,
    getters,
    mutations,
    actions
};

export default module;
