

module.exports = {
    owns (model, prop = 'created_by') {
        return (model[prop]) === App.user.name;
    },
    //dont update your own
    userOwner(model, prop = 'id') {
        return (model[prop] === App.user.id)
    }
};
