<!DOCTYPE html>
<html lang="en">
	<head>
	  	<meta charset="utf-8">
	  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	  	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	  	<link href="{{ mix("css/app.css") }}" rel="stylesheet"/>
		<link href="/css/style.css" rel="stylesheet">
		<meta name="google" value="notranslate">
		<meta name="csrf-token" content="{{ csrf_token() }}">
	  	<title>Login</title>
	</head>
	<body class="app flex-row align-items-center">
	  	<div class="container">
	    	<div class="row justify-content-center">
	      		<div class="col-md-8">
	        		<div class="card-group">
	          			<div class="card p-4">
	            			<div class="card-body">
	              				<h1>EngageIQ</h1>
	              				<p class="text-muted">Sign In to your account</p>
								<form class="" method="POST" action="{{ route('login') }}">
									{{csrf_field()}}
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="icon-user"></i></span>
										</div>
										<input id="email" name="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required autofocus>
										@if ($errors->has('email'))
	                                        <span class="invalid-feedback">
	                                        <strong>{{ $errors->first('email') }}</strong>
	                                    </span>
	                                    @endif
									</div>
									<div class="input-group mb-4">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="icon-lock"></i></span>
										</div>
										<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
										@if ($errors->has('password'))
	                                        <span class="invalid-feedback">
	                                        <strong>{{ $errors->first('password') }}</strong>
	                                    </span>
	                                    @endif
									</div>
									<div class="row">
										<div class="col-6">
											<button type="submit" class="btn btn-primary px-4">Login</button>
										</div>
										<div class="col-6 text-right">
											<a class="btn btn-link" href="{{ route('password.request') }}">
	                                        	Reset Your Password
	                                    	</a>
										</div>
									</div>
								</form>
	            			</div>
	          			</div>
	      				<div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%">
	        				<div class="card-body text-center">
	          					<div>
	            					<h2>About EngageIQ</h2>
	            					<p>Engage IQ has been producing results for our corporate partners since 2005. We’re located in the heart of Silicon Valley — the technology mecca of California and the world. We live and breathe both successful online marketing and the challenge of making our partners and users very happy! We are a growing company of professionals with exceptional team members in the U.S., India, and The Philippines.</p>
	            					<button type="button" class="btn btn-primary active mt-3">EngageIQ 2018</button>
	          					</div>
	        				</div>
	      				</div>
	        		</div>
	      		</div>
	    	</div>
	  	</div>

	  	<script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<script src="/js/main.js"></script>
		<!-- <script src="{{ mix("js/app.js") }}"></script> -->
	</body>
</html>
