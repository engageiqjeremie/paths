<!DOCTYPE html>
<html lang="en">
	<head>
	  	<meta charset="utf-8">
	  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	  	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	  	<link href="{{ mix("css/app.css") }}" rel="stylesheet"/>
		<link href="/css/style.css" rel="stylesheet">
		<meta name="google" value="notranslate">
		<meta name="csrf-token" content="{{ csrf_token() }}">
	  	<title>Send Email Password Reset</title>
	</head>
	<body class="app flex-row align-items-center">
	  	<div class="container">
	    	<div class="row justify-content-center">
	      		<div class="col-md-8">
	        		<div class="card-group">
	          			<div class="card p-4">
	            			<div class="card-body">
	              				<h1>EngageIQ</h1>
	              				<p class="text-muted">Sign In to your account</p>
								@if (session('status'))
								   <div class="alert alert-success">
									   {{ session('status') }}
								   </div>
							   @endif
							   <form class="" method="POST" action="{{ route('password.email') }}">
								   {{ csrf_field() }}
								   <div class="input-group mb-3">
									   <div class="input-group-prepend">
										   <span class="input-group-text"><i class="fa fa-envelope"></i></span>
									   </div>
									   <input placeholder="email" id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
									   @if ($errors->has('email'))
										   <span class="invalid-feedback">
											   <strong>{{ $errors->first('email') }}</strong>
										   </span>
									   @endif
								   </div>
								   <div class="form-group">
									   <div class="col-md-6 col-md-offset-4">
										   <button type="submit" class="btn btn-primary">
											   Send Password Reset Link
										   </button>
									   </div>
								   </div>
							   </form>
	            			</div>
	          			</div>
	      				<div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%">
	        				<div class="card-body text-center">
	          					<div>
	            					<h2>About EngageIQ</h2>
	            					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
	            					<button type="button" class="btn btn-primary active mt-3">EngageIQ 2018</button>
	          					</div>
	        				</div>
	      				</div>
	        		</div>
	      		</div>
	    	</div>
	  	</div>

	  	<script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<script src="/js/main.js"></script>
		<!-- <script src="{{ mix("js/app.js") }}"></script> -->
	</body>
</html>
