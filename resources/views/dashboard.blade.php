@extends('layouts/master')
@section('content')
    <div class="dashboard">
        <router-view></router-view>
    </div>
@endsection

@push('laravel_to_vue_script')
    @php
        $user = Auth::user();
    @endphp
    <script>
        window.App = {!! json_encode([
            'csrfToken' => csrf_token(),
            // 'user' => Auth::user(),
            'username' => Auth::user()->username,
            'gravatar' => Gravatar::get(Auth::user()->email),
            'api_token' => Auth::user()->api_token,
            'signedIn' => Auth::check(),
            'role' => Auth::user()->getRoleNames()
        ]) !!};
    </script>
@endpush
