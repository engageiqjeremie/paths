@extends('templates/' . $options['themes']['template'] . '/main')

@section('content')
<div class="card-body">
    {{ csrf_field() }}
    <input type="hidden" id="campaign_url" value="{{ $campaign_url }}">
    <input type="hidden" id="current_stack" value="{{ $current_stack }}">
    <input type="hidden" id="next_stack" value="{{ $next_stack }}">
    <input type="hidden" id="user_phone" value="{{ $phone }}">
    <input type="hidden" id="user_address" value="{{ $address }}">
    <div class="col-xl-10 col-lg-10 col-md-12 col-sm-12" style="margin-left: auto; margin-right: auto">
        @php
        //echo html_entity_decode($content, ENT_HTML5);
        eval("?>" . $content . "<?php;")
        @endphp
    </div>
</div>
@endsection

@push('script')
    <script src="/js/additional-methods.min.js"></script>
    <script>
    $(function () {
        // $('input[name="phone"]').mask('000-000-0000');
    });
    </script>
@endpush
