@component('mail::message')
# You have been given access to Automated Path Project - EngageIQ
### Hi {{$user->name}},

We just need you to confirm your email address to prove that you're a human, living in our planet Earth and is part of EngageIQ Team. You get it, right? Cool.

@component('mail::panel')
Use the auto-generated password below to login. Remember to change your password regularly.
@endcomponent

@component('mail::panel')
{{$password}}
@endcomponent

@component('mail::button', ['url' => url('/register/confirm?token=' . $user->confirmation_token)])
Confirm Email
@endcomponent
Thanks,<br>
{{ config('app.name') }}/ Davao Development Team
@endcomponent
