<!DOCTYPE html>
<html lang="en">
  	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <meta name="description" content="CoreUI Bootstrap 4 Admin Template">
	    <meta name="author" content="Lukasz Holeczek">
	    <meta name="keyword" content="CoreUI Bootstrap 4 Admin Template">
	    <title>Page Not Found</title>
	    <link href="{{ mix("css/app.css") }}" rel="stylesheet"/>
	    <link href="/css/style.css" rel="stylesheet">
  	</head>
	<body class="app flex-row align-items-center">
	  	<div class="container">
	    	<div class="row justify-content-center">
	      		<div class="col-md-6">
	        		<h1><a href="http://www.engageiq.com">EngageIQ</a></h1>
	        		<div class="clearfix">
	          			<h1 class="float-left display-3 mr-4">404 </h1>
	          			<h4 class="pt-3">Oops! You're lost.</h4>
	          			<p class="text-muted">{{ $exception->getMessage() }}</p>
	        		</div>
	      		</div>
	    	</div>
	  	</div>
	  	<script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	  	<script src="/js/main.js"></script>
	</body>
</html>
