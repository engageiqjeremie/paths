<footer class="app-footer">
    <span><a href="http://engageiq.com">EngageIQ</a> © <?php echo date("Y") ?></span>
    <span class="ml-auto">Powered by <a href="http://engageiq.com">EngageIQ</a></span>
</footer>
