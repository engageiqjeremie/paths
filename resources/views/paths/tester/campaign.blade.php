@extends('default.path.campaign')

@section('stylesheet')
<link href="/css/paths/tester.min.css?{{ $options['style']['version'] }}" rel="stylesheet">
@endsection
