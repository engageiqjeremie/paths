@extends('templates/' . $options['themes']['template'] . '/main')

@section('stylesheet')
<link href="/css/preview/{{ $options['themes']['template'] }}.min.css?{{ $options['style']['version'] }}" rel="stylesheet">
@endsection

@section('content')
<div class="card-body">

    <div id="ui-view" style="opacity: 1;">

        <div class="card">
            <div class="card-header">
                Headings
            </div>
            <div class="card-body">
                <p>Documentation and examples for {{ $options['themes']['template'] }} typography, including global settings, headings, body text, lists, and more.</p>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Heading</th>
                            <th>Example</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <p>
                                    <code class="highlighter-rouge">&lt;h1&gt;&lt;/h1&gt;</code>
                                </p>
                            </td>
                            <td>
                                <span class="h1">h1. {{ $options['themes']['template'] }} heading</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>
                                    <code class="highlighter-rouge">&lt;h2&gt;&lt;/h2&gt;</code>
                                </p>
                            </td>
                            <td>
                                <span class="h2">h2. {{ $options['themes']['template'] }} heading</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>
                                    <code class="highlighter-rouge">&lt;h3&gt;&lt;/h3&gt;</code>
                                </p>
                            </td>
                            <td>
                                <span class="h3">h3. {{ $options['themes']['template'] }} heading</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>
                                    <code class="highlighter-rouge">&lt;h4&gt;&lt;/h4&gt;</code>
                                </p>
                            </td>
                            <td>
                                <span class="h4">h4. {{ $options['themes']['template'] }} heading</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>
                                    <code class="highlighter-rouge">&lt;h5&gt;&lt;/h5&gt;</code>
                                </p>
                            </td>
                            <td>
                                <span class="h5">h5. {{ $options['themes']['template'] }} heading</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>
                                    <code class="highlighter-rouge">&lt;h6&gt;&lt;/h6&gt;</code>
                                </p>
                            </td>
                            <td>
                                <span class="h6">h6. {{ $options['themes']['template'] }} heading</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Inline text elements
            </div>
            <div class="card-body">
                <p>Traditional heading elements are designed to work best in the meat of your page content. When you need a heading to stand out, consider using a
                    <strong>display heading</strong>â€”a larger, slightly more opinionated heading style.</p>
                    <div class="bd-example">
                        <p>You can use the mark tag to
                            <mark>highlight</mark> text.</p>
                            <p>
                                <del>This line of text is meant to be treated as deleted text.</del>
                            </p>
                            <p>
                                <s>This line of text is meant to be treated as no longer accurate.</s>
                            </p>
                            <p>
                                <ins>This line of text is meant to be treated as an addition to the document.</ins>
                            </p>
                            <p><u>This line of text will render as underlined</u></p>
                            <p>
                                <small>This line of text is meant to be treated as fine print.</small>
                            </p>
                            <p>
                                <strong>This line rendered as bold text.</strong>
                            </p>
                            <p>
                                <em>This line rendered as italicized text.</em>
                            </p>
                        </div>
                    </div>
                </div>

        <div class="card">
            <div class="card-header">
                <i class="icon-drop"></i> Theme colors
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="p-3 mb-3 bg-primary">Primary</div>
                    </div>
                    <div class="col-md-4">
                        <div class="p-3 mb-3 bg-secondary">Secondary</div>
                    </div>
                    <div class="col-md-4">
                        <div class="p-3 mb-3 bg-success">Success</div>
                    </div>
                    <div class="col-md-4">
                        <div class="p-3 mb-3 bg-danger">Danger</div>
                    </div>
                    <div class="col-md-4">
                        <div class="p-3 mb-3 bg-warning">Warning</div>
                    </div>
                    <div class="col-md-4">
                        <div class="p-3 mb-3 bg-info">Info</div>
                    </div>
                    <div class="col-md-4">
                        <div class="p-3 mb-3 bg-light">Light</div>
                    </div>
                    <div class="col-md-4">
                        <div class="p-3 mb-3 bg-dark">Dark</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Progress
                <small>striped</small>
            </div>
            <div class="card-body">
                <div class="progress mb-3">
                    <div class="progress-bar progress-bar-striped bg-primary" role="progressbar" style="width: 10%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="progress mb-3">
                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="progress mb-3">
                    <div class="progress-bar progress-bar-striped bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="progress mb-3">
                    <div class="progress-bar progress-bar-striped bg-warning" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="progress mb-3">
                    <div class="progress-bar progress-bar-striped bg-danger" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                Progress
                <small>animated</small>
            </div>
            <div class="card-body">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
                </div>
            </div>
        </div>

        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <strong>Options</strong>
                        </div>
                        <div class="card-body">

                            <button type="button" class="btn btn-primary">Primary</button>

                            <button type="button" class="btn btn-secondary">Secondary</button>

                            <button type="button" class="btn btn-success">Success</button>

                            <button type="button" class="btn btn-warning">Warning</button>

                            <button type="button" class="btn btn-danger">Danger</button>

                            <button type="button" class="btn btn-link">Link</button>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <strong>Options</strong>
                        </div>
                        <div class="card-body">

                            <button type="button" class="btn btn-outline-primary">Primary</button>

                            <button type="button" class="btn btn-outline-secondary">Secondary</button>

                            <button type="button" class="btn btn-outline-success">Success</button>

                            <button type="button" class="btn btn-outline-warning">Warning</button>

                            <button type="button" class="btn btn-outline-danger">Danger</button>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

</div>

@endsection

@push('script')
<script>
$(function() {

});
</script>
@endpush
