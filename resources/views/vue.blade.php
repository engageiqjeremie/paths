<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Feedback</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <style media="screen">
            .app-footer { display: none; }
        </style>
    </head>
    <body>
        <div id="feedback" class="feedback">
            <router-view></router-view>
        </div>

        @include('includes.footer')

        <script>
            window.Csrf = '{!! csrf_token() !!}';
        </script>
        <script src="/js/feedback.js"></script>
    </body>
</html>
