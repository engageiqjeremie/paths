<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Api')->group(function () {
    /**
     * User/Roles/auth related routes
     */
    Route::get('/all-users', 'UserController@index');
    Route::get('/get-user/{userID}', 'UserController@getUser');
    Route::get('/get-user-roles', 'UserController@roles');
    Route::get('/get-roles-permissions', 'UserController@permissions');
    Route::get('/all-users', 'UserController@index');
    Route::post('/create-user', 'UserController@createUser');
    Route::post('/edit-user/{id}', 'UserController@edit');
    Route::post('/deactivate-user/{id}', 'UserController@deactivateUser');
    Route::get('/get-user-roles', 'RoleController@roles');
    Route::post('/create-role', 'RoleController@createRoles');
    Route::post('/update-roles/{id}', 'RoleController@updateRoles');
    Route::post('/delete-role/{id}', 'RoleController@deleteRole');
    Route::get('/get-roles-permissions', 'PermissionController@permissions');
    Route::post('/create-permission', 'PermissionController@createPermissions');
    Route::post('/update-permission/{id}', 'PermissionController@updatePermission');
    Route::post('/delete-permission/{id}', 'PermissionController@deletePermission');

    /**
     * Paths
     */
    Route::get('/paths', 'PathController@lists');
    Route::post('/paths', 'PathController@create');
    Route::get('/paths/{id}', 'PathController@get');
    Route::put('/paths/{id}', 'PathController@update');
    Route::patch('/paths/{id}', 'PathController@updateStatus');
    Route::delete('/paths/{id}', 'PathController@delete');

    /**
     * Options
     */
    Route::post('/option-general/{id}', 'OptionController@optionGeneral');
    Route::post('/option-theme/{id}', 'OptionController@optionTheme');
    Route::post('/option-header/{id}', 'OptionController@optionHeader');
    Route::post('/option-footer/{id}', 'OptionController@optionFooter');
    Route::post('/option-progress-bar/{id}', 'OptionController@optionProgressBar');
    Route::post('/option-nav-bar/{id}', 'OptionController@optionNavigationBar');
    Route::post('/option-content/{id}', 'OptionController@optionContent');
    Route::post('/option-trackers/{id}', 'OptionController@optionTrackers');
    Route::post('/option-feedback/{id}', 'OptionController@optionFeedback');
    Route::post('/option-remove/{id}', 'OptionController@optionRemove');

    /**
     * Stylesheets
     */
    Route::get('/style/{slug}', 'StyleController@get');
    Route::post('/style/update/{id}', 'StyleController@update');
    Route::post('/style/backup/{slug}', 'StyleController@backupStylesheet');
    Route::post('/style/revert/{slug}', 'StyleController@revertStylesheetBackup');
    Route::post('/style/apply-reversion/{slug}', 'StyleController@applyReversionStylesheetBackup');
    Route::post('/style/apply-reversion/{id}', 'StyleController@applyReversionStylesheetBackup');
    Route::post('/style/cancel-reversion/{slug}', 'StyleController@cancelReversionStylesheetBackup');

    /**
     * Template
     */
     Route::get('/templates', 'TemplateController@templates');
     Route::get('/custom-progress-bar', 'TemplateController@customProgressBar');
     Route::get('/trackers', 'TemplateController@trackers');

     /**
      * Feedback
      */
     Route::get('/email-recipients', 'FeedbackController@emailRecipients');
     Route::any('/add-email-recipients', 'FeedbackController@addEmailRecipients');

     /**
      * Colors
      */

     Route::get('/colors', 'ColorController@colors');

    /**
     * bugs and comments
     */
    Route::resource('bugs', 'BugController');

    /**
     *  documentation in markdown format
     */
    Route::post('/project-documentation', 'ProjectDocumentationController@updateDocs');
    Route::get('/project-documentation', 'ProjectDocumentationController@docs');

    /**
     *  comments
     */
    Route::post('/submit-comments/{id}', 'CommentController@submitComment');
    Route::get('/all-related-comments/{id}', 'CommentController@getRelatedComments');
    Route::post('/delete-related-comment/{id}', 'CommentController@deleteRelatedComment');

    /**
     *  tasks
     */
    Route::get('/getAllUsersTasks', 'UsersTaskController@index');

});
