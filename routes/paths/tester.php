<?php

/*
 | ---------------------------------------------------------------------------------------------------------------------------------
 | Generated routes
 | ---------------------------------------------------------------------------------------------------------------------------------
 |
 */

 Route::get('/tester', 'Paths\TesterController@landing')->name('tester-landing')->middleware('nlrToken');
 // Route::get('/tester', 'Paths\TesterController@landing')->name('tester-landing');
 Route::get('/tester/registration', 'Paths\TesterController@registration')->name('tester-registration');
 Route::post('/tester/registration', 'Paths\TesterController@processRegistration');
 Route::get('/tester/question', 'Paths\TesterController@question')->name('tester-question');
 Route::post('/tester/question', 'Paths\TesterController@processQuestion');
 Route::get('/tester/campaign/{page}', 'Paths\TesterController@campaign')->name('tester-campaign');
