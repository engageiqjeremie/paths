<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class PathDatabaseTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDatabase()
    {
        factory(\App\Path::class)->create([
            'name' => 'path6',
            'url' => 'http://localhost:8000/path6'
        ]);
        $this->assertDatabaseHas('paths', [
            'name' => 'path6',
            'url' => 'http://localhost:8000/path6'
        ]);
    }
}
