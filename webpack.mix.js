let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');
mix.copy('resources/assets/js/assets/img', 'public/img');
mix.sass('resources/assets/sass/style.scss', 'public/css');

// mix.sass('resources/assets/sass/templates/preview/default.scss', 'public/css/preview/default.min.css');

mix.js('resources/assets/js/pages/path.js', 'public/js');
mix.js('resources/assets/js/pages/pathhead.js', 'public/js');
mix.js('resources/assets/js/pages/feedback.js', 'public/js');

mix.sass('resources/assets/sass/templates/preview/default.scss', 'public/css/preview/default.min.css');
